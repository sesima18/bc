@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    {{ trans('db.pageTitle') }}
@endcomponent
@endslot

{!! $body !!}

<b>{{ __('db.orderInfo') }}</b><br />
<b>{{ $order->name.' '.$order->surname }}</b><br />
@if(!empty($order->phone))
    {{ __('validation.attributes.phone') }}: <b>{{ $order->phone }}</b><br />
@endif
{{ __('validation.attributes.email') }}: <b>{{ $order->email }}</b><br />
{{ __('validation.attributes.address') }}: <b>{{ $order->address }}</b><br />
{{ __('validation.attributes.countryId') }}: <b>{{ $order->country }}</b><br />
{{ __('db.delivery') }}: <b>{{ $order->delivery }}</b><br />
@if($order->deliveryTerminal)
    {{ __('db.shippingTerminal') }}: <b>{{ $order->deliveryTerminal }}</b><br />
@endif
{{ __('db.payment') }}: <b>{{ $order->payment }}</b><br />
{{ __('db.message') }}: {{ $order->comment }}<br />
<br />

<b>{{ __('db.products') }}</b>
@component('mail::table')
    |{{ __('db.product') }} | {{ __('db.price') }} | {{ __('db.quantity') }} | {{ __('db.subtotal') }}  |
    |-------------|:-------------:|:--------:|--------:|
@foreach($order->products as $one)
| {{ $one->name }} | {{ $one->price  }} | {{ $one->quantity  }} | {{ $one->subtotal  }} |
@endforeach
@endcomponent

@component('mail::panel')
    {{ __('db.total') }}: {{ $order->total }}
@endcomponent

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    {{ trans('db.letterFooter') }}
@endcomponent
@endslot

@endcomponent

@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    {{ trans('db.pageTitle') }}
@endcomponent
@endslot

{!! $body !!}

{{ __('validation.attributes.user_name') }}: <b>{{ $customer->name }}</b>

{{ __('validation.attributes.email') }}: <b>{{ $customer->email }}</b>

{{ __('validation.attributes.phone') }}: <b>{{ $customer->phone }}</b>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    {{ trans('db.letterFooter') }}
@endcomponent
@endslot

@endcomponent

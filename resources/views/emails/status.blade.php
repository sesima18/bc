@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    {{ trans('db.pageTitle') }}
@endcomponent
@endslot

{!! $body !!}

{{ trans('db.orderStatusChanged') }} <b>{{ $order->statusName }}</b>

@if($order->status == 3)
{{ trans('db.trackCode') }}: <b>{{ $order->track_code }}</b>
@endif

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    {{ trans('db.letterFooter') }}
@endcomponent
@endslot

@endcomponent

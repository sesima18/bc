<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ADMIN | Baltic-collector</title>

        @vite('resources/js/src/main.ts')
    </head>
    <body class="sidebar-show bg-body-secondary">
    <div id="app"></div>
    </body>
</html>

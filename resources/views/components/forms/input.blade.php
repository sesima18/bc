<div class="form-group mb-15 mb-sm-20 mb-md-25">
@if($label)
    <label class="d-block text-black fw-semibold mb-10">{{ $label }}</label>
@endif
<input name="{{ $name }}" type="{{ $type }}" class="@error($name) is-invalid @elseif(!empty($value)) is-valid @enderror form-control shadow-none rounded-0 text-black" placeholder="{{ $placeholder }}"
       value="{{ $value }}"/>
</div>

@error($name)
<div class="invalid-feedback d-block">{{ $message }}</div>
@enderror

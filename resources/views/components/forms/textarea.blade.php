<div class="form-group mb-15 mb-sm-20 mb-md-25">
    @if($label)
        <label class="d-block text-black fw-semibold mb-10">{{ $label }}</label>
    @endif
    <textarea name="{{ $name }}" cols="5" rows="3"
              class="form-control shadow-none rounded-0 text-black"
              placeholder="{{ $placeholder }}">{{ $value }}</textarea>
</div>

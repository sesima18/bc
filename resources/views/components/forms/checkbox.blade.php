<div class="form-group mb-15 mb-sm-20 mb-md-25">
    <input name="{{ $name }}" id="{{ $id }}" type="checkbox" class="@error($name) is-invalid @elseif(!empty($value)) is-valid @enderror"
           value="{{ $value }}" @if($checked) checked @endif />
    @if($label)
        <label for="{{ $id }}" class="ms-1 text-black fw-semibold mb-10">{{ $label }}</label>
    @endif
</div>

@error($name)
<div class="invalid-feedback d-block">{{ $message }}</div>
@enderror

<div class="form-group mb-15 mb-sm-20 mb-md-25">
    @if($label)
        <label class="d-block text-black fw-semibold mb-10">{{ $label }}</label>
    @endif
    <select name="{{ $name }}"
            class="@error($name) is-invalid @elseif(!empty($value)) is-valid @enderror  form-select shadow-none fw-semibold rounded-0">
        @if($placeholder)
            <option value="">{{ $placeholder }}</option>
        @endif
        @foreach($options as $groupName => $groupOption)
            @if(is_array($groupOption))
                <optgroup label="{{ $groupName }}">
                    @foreach($groupOption as $option)
                        <option
                            value="{{ $option->id }}" {{ ($option->id == $value) ? 'selected' : '' }}>{{ $option->name }}</option>
                    @endforeach
                </optgroup>
            @else
                <option
                    value="{{ $groupOption->id }}" {{ ($groupOption->id == $value) ? 'selected' : '' }}>{{ $groupOption->name }}</option>
            @endif
        @endforeach
    </select>
</div>

@error($name)
<div class="invalid-feedback d-block">{{ $message }}</div>
@enderror

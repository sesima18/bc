@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => $currentCategory->name])

            @if($currentCategory->categoryType == \App\Enums\CategoryTypes::CONTACTS)


                <form method="POST" action="{{ route('contacts', $currentLocale) }}" data-contact-from>
                    @csrf
                    <div class="row">
                        <div class="col-6"> {!! $currentCategory->body !!}</div>
                        <div class="col-6">
                            <h5 class="mb-15 mb-md-25 fw-bold text-black mb">{{ trans('db.contactForma') }}</h5>

                            <div data-contact-from-success class="alert alert-success"
                                  style="display: none">{{ trans('db.contactFormSuccess') }}</div>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <x-forms.input name="name" :label="__('validation.attributes.user_name')"  />
                                </div>
                                <div class="col-md-6 mb-3">
                                    <x-forms.input name="email" type="email" :label="__('validation.attributes.email')" />
                                </div>
                                <div class="col-md-12 mb-3">
                                    <x-forms.textarea name="message" :label="__('db.message')" />
                                </div>
                            </div>
                            <button type="submit" class="btn btn-gold pt-2 pb-2 ps-4 pe-4">
                                {{ trans('db.send') }} <i class="fa-solid fa-arrow-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            @else
                {!! $currentCategory->body !!}
            @endif
        </div>
    </div>

@endsection



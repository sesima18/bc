@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => $currentCategory->name.': '.$customerData->name])

            <div class="row">
                <div class="col-6">
                    <div class="d-flex justify-content-between mb-2">
                        <h5 class="mb-15 mb-md-25 fw-bold text-black">{{ trans('db.profile') }}</h5>
                        <a class="btn btn-gold btn-sm" href="{{ route('logout', $currentLocale) }}">
                            {{ trans('db.logout') }} <i class="fa-solid fa-arrow-right-from-bracket ms-1"></i>
                        </a>
                    </div>
                    @if(Session::has('profileSuccess'))
                        <div class="alert alert-success">{{ trans('db.profileUpdateSuccess') }}</div>
                    @endif
                    <form method="POST" action="{{ route('profile', $currentLocale) }}">
                        @csrf
                        <div class="row">
                            @include('web.customer.form')
                        </div>
                        <button type="submit" class="btn btn-gold">
                            {{ trans('db.send') }} <i class="fa-solid fa-arrow-right ms-1"></i>
                        </button>
                    </form>
                </div>
                <div class="col-6">
                    @if(isset($orders) and count($orders) > 0)
                    <h5 class="mb-15 mb-md-25 fw-bold text-black mb-2">{{ trans('db.orderHistory') }}</h5>

                    <table class="table text-nowrap align-middle mb-0">
                        <thead>
                        <tr>
                            <th scope="col">{{ trans('db.orderNo') }}</th>
                            <th scope="col" class="text-center">{{ trans('db.status') }}</th>
                            <th scope="col" class="text-center">{{ trans('db.total') }}</th>
                            <th scope="col" class="text-center">{{ trans('db.delivery') }}</th>
                            <th scope="col" class="text-end">{{ trans('db.products') }}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>#{{ $order->id }}</td>
                                <td class="text-center"><span
                                        class="status-badge btn-sm btn {{ $order->statusClass }}">{{ $order->status }}</span>
                                </td>
                                <td class="text-center"> {{ $order->total }}</td>
                                <td class="text-center">
                                    {{ $order->delivery }}
                                    @if(!empty($order->deliveryTerminal)) <small class="d-block"> {{ trans('db.shippingTerminal') }}: {{ $order->deliveryTerminal }} </small> @endif
                                    @if(!empty($order->trackCode)) <small class="d-block"> {{ trans('db.trackCode') }}: {{ $order->trackCode }} </small> @endif
                                </td>
                                <td class="text-end">
                                    <div class="dropdown">
                                        <a href="#" class="nav-link dropdown-toggle" type="button"
                                           id="dropdownMenuButton-{{ $order->id }}" data-bs-toggle="dropdown"
                                           aria-expanded="false">
                                            {{ trans('db.products') }}
                                        </a>
                                        <ul class="dropdown-menu p-2 pt-0"
                                            aria-labelledby="dropdownMenuButton-{{ $order->id }}">
                                            @foreach($order->products as $cartItem)
                                                <li class="mt-2">
                                                    <a href="{{ $cartItem->link }}" class="d-block link-gold">{{ $cartItem->name }}</a>
                                                    {{ $cartItem->price }} x {{ $cartItem->quantity }} {{ trans('db.units') }}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>

@endsection

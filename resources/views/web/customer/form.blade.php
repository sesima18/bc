<div class="col-md-12 mb-3">
    <x-forms.input name="name" :label="__('validation.attributes.user_name')" :value="$customerData->name" />
</div>
<div class="col-md-12 mb-3">
    <x-forms.input name="surname" :label="__('validation.attributes.surname')" :value="$customerData->surname" />
</div>
<div class="col-md-12 mb-3">
    <x-forms.input name="email" type="email" :label="__('validation.attributes.email')" :value="$customerData->email" />
</div>
<div class="col-md-12 mb-3">
    <x-forms.input name="phone" :label="__('validation.attributes.phone')" :value="$customerData->phone"/>
</div>
<div class="col-md-12 mb-3">
    <x-forms.input name="address" :label="__('validation.attributes.address')" :value="$customerData->address" />
</div>
<div class="col-md-12 mb-3">
    <x-forms.select name="countryId" :options="$countries" :label="__('validation.attributes.countryId')" :value="$customerData->countryId" />
</div>
<div class="col-md-12 mb-3">
    <x-forms.checkbox value="1" name="noSubscription" id="noSubscription" :label="__('db.noSubscription')" :checked="$customerData->noSubscription" />
</div>


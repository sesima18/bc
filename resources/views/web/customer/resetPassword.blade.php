@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => trans('db.resetPassword') ])

            <div class="row">
                @if(Session::has('resetPasswordSuccess'))
                    <div class="alert alert-success">{{ trans('db.resetPasswordSuccess') }}</div>
                @endif
                <form method="POST" action="{{ route('resetPassword', $currentLocale) }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}" />
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <x-forms.input name="email" type="email" :label="__('validation.attributes.email')"/>
                        </div>
                        <div class="col-md-6 mb-3">
                            <x-forms.input type="password" name="password"
                                           :label="__('validation.attributes.password')"/>
                        </div>
                        <div class="col-md-6 mb-3">
                            <x-forms.input type="password" name="password_confirmation"
                                           :label="__('validation.attributes.password_confirmation')"/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-gold">
                        {{ trans('db.send') }} <i class="fa-solid fa-arrow-right ms-1"></i>
                    </button>
                </form>
            </div>

        </div>
    </div>
    </div>

@endsection

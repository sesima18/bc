@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => $currentCategory->name])

            <div class="row">
                <div class="col-6">
                    <h5 class="fw-bold mb-4">{{ trans('db.login') }}</h5>
                    <form method="POST" action="{{ route('login', $currentLocale) }}">
                        @if(Session::has('loginError'))
                            <div class="alert alert-danger">{{ trans('validation.auth') }}</div>
                        @endif
                        @csrf
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <x-forms.input name="email" type="email" :label="__('validation.attributes.email')"/>
                            </div>
                            <div class="col-md-12 mb-3">
                                <x-forms.input type="password" name="password"
                                               :label="__('validation.attributes.password')"/>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <button type="submit" class="btn btn-gold pt-2 pb-2 ps-4 pe-4">
                                {{ trans('db.login') }} <i class="fa-solid fa-arrow-right"></i>
                            </button>
                            <a href="{{ $currentCategory->link }}/forgot-password" class="link-gold pt-2">{{ trans('db.forgotPassword') }}</a>
                        </div>
                    </form>
                </div>
                <div class="col-6">
                    <h5 class="fw-bold mb-4">{{ trans('db.register') }}</h5>
                    <form method="POST" action="{{ route('register', $currentLocale) }}">
                        @csrf
                        <div class="row">
                            @include('web.customer.form')
                            <div class="col-md-12 mb-3">
                                <x-forms.input type="password" name="password"
                                               :label="__('validation.attributes.password')"/>
                            </div>
                            <div class="col-md-12 mb-3">
                                <x-forms.input type="password" name="password_confirmation"
                                               :label="__('validation.attributes.password_confirmation')"/>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-gold pt-2 pb-2 ps-4 pe-4">
                            {{ trans('db.register') }} <i class="fa-solid fa-arrow-right"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

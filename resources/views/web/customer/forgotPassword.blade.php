@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => trans('db.forgotPassword') ])

            <div class="row">
                @if(Session::has('forgotPasswordSuccess'))
                    <div class="alert alert-success">{{ trans('db.forgotPasswordSuccess') }}</div>
                @endif
                <form method="POST" action="{{ route('forgotPassword', $currentLocale) }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <x-forms.input name="email" type="email" :label="__('validation.attributes.email')"/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-gold">
                        {{ trans('db.send') }} <i class="fa-solid fa-arrow-right ms-1"></i>
                    </button>
                </form>
            </div>

        </div>
    </div>
    </div>

@endsection

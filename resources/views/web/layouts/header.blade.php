<header>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-3 col-12">
                <form action="{{ $searchCategory->link }}" class="d-flex">
                    <input class="form-control me-2" type="search" name="search" placeholder="{{ trans('db.search') }}"
                           aria-label="Search" value="{{ $search ?? ''  }}"/>
                    <button type="submit"><i class="fa-solid fa-magnifying-glass"></i></button>
                </form>
            </div>
            <div class="offset-sm-1 col-sm-4 col-12 mt-sm-0 mt-3 mb-sm-0 mb-3 text-center logo cursor-pointer">
                <h1 data-heading="Baltic collector" contenteditable
                    onclick="window.location = '/{{ $currentLocale }}'">{{ trans('db.pageTitle') }}</h1>
            </div>
            <div class="col-sm-4 col-12 text-end">
                <a class="me-xl-5 d-block d-xl-inline" aria-current="page" href="{{ $customerCategory->link }}">
                    <i class="fa-solid fa-user"></i> <span class="d-lg-inline-block d-none">
                        {{ ($currentCustomer) ? $currentCustomer->name : trans('db.login') }}
                    </span>
                </a>
                <a aria-current="page" href="{{ $cartCategory->link }}">
                    <i class="fa-solid fa-bag-shopping"></i> <span
                        class="d-lg-inline-block d-none">{{ trans('db.cart') }}</span>
                    (<span class="cart-total" data-cart-info>{{ $cartTotal }}</span>)
                </a>
            </div>
        </div>
    </div>
</header>

<div class="container top-menu mt-3 mb-3">
    <div class="d-flex justify-content-between align-items-center">
        <div>
            <ul class="nav">
                @foreach($menuHeader as $menu)
                    <li class="nav-item @if(count($menu->submenu) > 0) dropdown @endif">
                        @if(count($menu->submenu) > 0)
                            <a class="nav-link dropdown-toggle {{ $menu->isInPath ? 'current' : '' }}"
                               href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">{{ $menu->name }}</a>
                            <ul class="dropdown-menu pb-0">
                                @foreach($menu->submenu as $menu2)
                                    <li><a class="dropdown-item mb-2" href="{{ $menu2->link }}">{{ $menu2->name }}</a></li>
                                @endforeach
                            </ul>
                        @else
                            <a class="nav-link {{ $menu->isInPath ? 'current' : '' }}" aria-current="page"
                               href="{{ $menu->link }}">{{ $menu->name }}</a>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="langs">
            @foreach($languages as $language)
                <a href="/{{ $language->name }}" class="ms-md-2 ms-3">
                    <img src="https://www.vikant.lt/images/{{ $language->name }}.png" alt="en">
                </a>
            @endforeach
        </div>
    </div>
</div>

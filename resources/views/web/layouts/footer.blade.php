<footer id="footer" class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="footer-column__title footer-column__title--button-collapsed title collapsed"
                     data-toggle="collapse" data-target="#fc1">{{ trans('db.menuFooter1') }}
                </div>
                <ul id="fc1" class="footer-navigation">
                    @foreach($menuFooter1 as $menu)
                        <li class="footer-navigation__item mb-2">
                            <a href="{{ $menu->link }}" class="footer-navigation__link">{{ $menu->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <div class="footer-column__title footer-column__title--button-collapsed title collapsed"
                     data-toggle="collapse" data-target="#fc2">{{ trans('db.menuFooter2') }}
                </div>
                <ul id="fc2" class="footer-navigation">
                    @foreach($menuFooter2 as $menu)
                        <li class="footer-navigation__item mb-2">
                            <a href="{{ $menu->link }}" class="footer-navigation__link">{{ $menu->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <div class="footer-column__title footer-column__title--button-collapsed title collapsed"
                     data-toggle="collapse" data-target="#fc3">{{ trans('db.menuFooter3') }}
                </div>
                <ul id="fc3" class="footer-navigation">
                    @foreach($productsMenu as $menu)
                        <li class="footer-navigation__item mb-2">
                            <a href="{{ $menu->link }}" class="footer-navigation__link">{{ $menu->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <div class="footer-column__title footer-column__title--button-collapsed title collapsed"
                     data-toggle="collapse" data-target="#fc4">{{ trans('db.menuFooter4') }}
                </div>
                <ul id="fc4" class="footer-navigation list-unstyled">
                    <li class="footer-navigation__item footer-navigation__item--phone mb-2">
                        <a href="tel:{{ trans('db.pagePhone') }}" class="footer-navigation__link">
                            <i class="es es--phone fa fa-phone me-2" aria-hidden="true"></i> {{ trans('db.pagePhone') }}
                        </a>
                    </li>
                    <li class="footer-navigation__item footer-navigation__item--email mb-2">
                        <a href="mailto:{{ trans('db.pageEmail') }}t" class="footer-navigation__link">
                            <i class="es es--mail fa fa-envelope me-2" aria-hidden="true"></i> {{ trans('db.pageEmail') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="footer__copyright copyright mt-2">{!! trans('db.copyright.html') !!}</div>
    </div>
</footer>

<div class="text-start">{{ $product->id }}</div>
<figure class="image cart-img-{{ $product->id }}">
    <a href="{{ $product->link }}">
        <img src="{{ $product->photo }}" alt="{{ $product->name }}"/>
    </a>
    @if($product->isTop)
        <span>!</span>
    @elseif($product->isBest)
        <span>%</span>
    @elseif($product->isNew)
        <span>n</span>
    @endif
</figure>
<h3><a href="{{ $product->link }}">{{ $product->name }}</a></h3>
<div class="mb-3">
    <div class="{{ ($product->saleMoney) ? "price-sale" : "price" }}">
        @if($product->saleMoney)
            <del>{{ $product->saleMoney->price }}</del> €
        @endif
            <span>{{ $product->priceMoney }}</span>
    </div>
</div>
<div class="text-center">
    <a href="{{ $product->link }}" class="btn btn-outline-dark btn-more me-3">{{ trans('db.more') }}</a>
    @if($product->quantity > 0)
        <a href="/cart/{{ $product->id }}/1" class="btn btn-gold" data-add-to-cart="{{ $product->id }}">{{ trans('db.addToCart') }}</a>
    @else
        <span class="btn btn-light">{{ trans('db.soldOut') }}</span>
    @endif
</div>

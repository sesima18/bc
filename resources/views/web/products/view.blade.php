@extends('web.layouts.app')

@section('content')

    <div class="container product-view">
        <div class="row">
            <div class="col-3 menu">
                @include('web.partials.productsMenu', ['title' => trans('db.categories')])
            </div>
            <div class="col-9">
                <div class="block-categories-slider">
                    @include('web.partials.title', ['title' => $currentProduct->name])

                    <div class="row mb-3">
                        <div class="col-6">
                            <figure class="image cart-img-{{ $currentProduct->id }}">
                                @foreach($currentProduct->photos as $photo)
                                    <div
                                        data-photo="{{ $loop->index }}" {{ ($loop->index > 0) ? 'style=display:none' : '' }}>
                                        <a href="{{ $photo["photoUrl"] }}" data-lightbox="image-1">
                                            <img src="{{ $photo["photoUrl"] }}" alt="{{ $currentProduct->name }}"/>
                                        </a>
                                    </div>
                                @endforeach
                                @if($currentProduct->isTop)
                                    <span>!</span>
                                @elseif($currentProduct->isBest)
                                    <span>%</span>
                                @elseif($currentProduct->isNew)
                                    <span>n</span>
                                @endif
                            </figure>
                            @if(count($currentProduct->photos) > 1)
                                <div class="row">
                                    @foreach($currentProduct->photos as $photo)
                                        <div class="col-4">
                                            <a href="#" data-thumb="{{ $loop->index }}">
                                                <img src="{{ $photo["photoUrl"] }}" alt="{{ $currentProduct->name }}"
                                                     class="img-thumbnail {{ ($loop->index == 0) ? 'select-img-thumbnail' : '' }}"/>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="col-6">
                            <p>{{ trans('db.id') }}: <b>{{ $currentProduct->id }}</b></p>
                            <div class="mt-3 mb-3">
                                {{ trans('db.price') }}: <div class="d-inline-block {{ ($currentProduct->saleMoney) ? "price-sale" : "price" }}">
                                    @if($currentProduct->saleMoney)
                                        <del>{{ $currentProduct->saleMoney->price }}</del> €
                                    @endif
                                    <span>{{ $currentProduct->priceMoney }}</span>
                                </div>
                            </div>
                            @if($currentProduct->quantity > 0)
                            <a href="/cart/{{ $currentProduct->id }}/1" class="btn btn-gold mb-3"
                               data-add-to-cart="{{ $currentProduct->id }}">{{ trans('db.addToCart') }}</a>
                            @else
                                <span class="btn btn-light mb-3">{{ trans('db.soldOut') }}</span>
                            @endif
                            {!! $currentProduct->body !!}
                        </div>
                    </div>


                    @include('web.partials.title', ['title' => trans('db.relatedProducts')])
                    <div class="row mt-5">
                        @foreach($relatedProducts as $product)
                            <div class="col-4 product">
                                @include('web.products.product')
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection



@extends('web.layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 menu">
                @include('web.partials.productsMenu', ['title' => trans('db.categories')])
            </div>
            <div class="col-lg-9 col-md-8 col-sm-6">
                <div class="block-categories-slider">
                    @include('web.partials.title', ['title' => "$currentCategory->name: $search"])
                    {!! $currentCategory->body !!}

                    @if(count($products) > 0)

                    <div class="row mt-5">
                        @foreach($products as $product)
                            <div class="col-lg-4 col-md-6 product">
                                @include('web.products.product')
                            </div>
                        @endforeach
                    </div>

                        {{  $products->links('web.partials.pagination') }}
                    @else

                        <div class="text-center p-5">
                            <div class="alert alert-danger">{!! trans('db.searchEmpty') !!}</div>
                        </div>

                    @endif

                </div>
            </div>
        </div>
    </div>


@endsection



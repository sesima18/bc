@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => $currentCategory->name])

            @if(Cart::count() > 0)

                <form method="POST" action="{{ route('checkout', $currentLocale) }}">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <h5 class="mb-15 mb-md-25 fw-bold text-black mb">{{ trans('db.orderInfo') }}</h5>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <x-forms.input name="name" :label="__('validation.attributes.user_name')"
                                                   :value="$checkoutData->name"/>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <x-forms.input name="surname" :label="__('validation.attributes.surname')"
                                                   :value="$checkoutData->surname"/>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <x-forms.input name="email" type="email" :label="__('validation.attributes.email')"
                                                   :value="$checkoutData->email"/>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <x-forms.input name="phone" :label="__('validation.attributes.phone')"
                                                   :value="$checkoutData->phone"/>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <x-forms.input name="address" :label="__('validation.attributes.address')"
                                                   :value="$checkoutData->address"/>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <x-forms.select name="countryId" :placeholder="__('validation.attributes.countryId')" :label="__('validation.attributes.countryId')" :options="$countries"
                                                    :value="$checkoutData->countryId"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <input type="hidden" name="deliveryId" data-delivery-id value="{{ $deliveryId }}"/>
                            <h5 class="mb-15 mb-md-25 fw-bold text-black">{{ trans('db.delivery') }}</h5>
                            <div class="row">
                                @foreach($deliveries as $delivery)
                                    <div class="col-6">
                                        <?php
                                            $categoryId = old('countryId', $checkoutData->countryId);
                                            $hide = true;
                                            if(empty($delivery->country_id)) {
                                                $hide = false;
                                            }
                                            if(in_array($categoryId, explode(',', $delivery->country_id))) {
                                                $hide = false;
                                            }
                                            ?>
                                        <div data-delivery-country="{{ $delivery->country_id }}"   @if($hide) style="display:none" @endif
                                            class="form-control accordion-item rounded-0 mb-3 @error('deliveryId') is-invalid @elseif($deliveryId == $delivery->id) is-valid @enderror ">
                                            <div data-delivery-choose="{{ $delivery->id }}"
                                                 class="accordion-button text-black fs-14 fs-md-15 fs-lg-16 fw-semibold shadow-none border-0 rounded-0 bg-white align-items-center justify-content-between collapsed"
                                                 type="button" data-bs-toggle="collapse"
                                                 data-bs-target="#paymentCollapseTwo"
                                                 aria-expanded="false" aria-controls="paymentCollapseTwo"
                                            ><span
                                                    class="dot @if($deliveryId == $delivery->id)  dot-selected @endif"></span> {{ $delivery->name }}
                                            </div>
                                            @if(count($delivery->terminals) > 0)
                                                <div class="mt-3"></div>
                                            <x-forms.select  name="deliveryTerminal[{{ $delivery->id }}]" :options="$delivery->terminals"
                                                            :placeholder="__('db.shippingTerminal')"
                                                            :value="$checkoutData->deliveryTerminalCode" />
                                                @endif
                                        </div>
                                    </div>
                                @endforeach
                                @error('deliveryId')
                                <div class="invalid-feedback d-block">{{ $message }}</div>
                                @enderror
                            </div>

                            <input type="hidden" name="paymentId" data-payment-id value="{{ $paymentId }}"/>
                            <h5 class="mb-15 mb-md-25 fw-bold text-black">{{ trans('db.payment') }}</h5>
                            <div class="row">
                                @foreach($payments as $payment)
                                    <div class="col-6">
                                        <div
                                            class="form-control accordion-item rounded-0 mb-3 @error('paymentId') is-invalid @elseif($paymentId == $payment->id) is-valid @enderror">
                                            <div data-payment-choose="{{ $payment->id }}"
                                                 class="accordion-button text-black fs-14 fs-md-15 fs-lg-16 fw-semibold shadow-none border-0 rounded-0 bg-white d-flex align-items-center justify-content-between collapsed"
                                                 type="button" data-bs-toggle="collapse"
                                                 data-bs-target="#paymentCollapseTwo"
                                                 aria-expanded="false" aria-controls="paymentCollapseTwo"><span
                                                    class="dot @if($paymentId == $payment->id)  dot-selected @endif"></span> {{ $payment->name }}
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @error('paymentId')
                            <div class="invalid-feedback d-block">{{ $message }}</div>
                            @enderror

                            <div class="col-md-12 mb-3">
                                <x-forms.textarea name="comment" :label="__('db.message')"
                                                  :value="$checkoutData->comment"/>
                            </div>

                        </div>
                    </div>

                    <div class="d-flex justify-content-between mt-3">
                        <a href="{{ $cartCategory->link }}" class="link-gold">
                            <i class="fa-solid fa-arrow-left"></i> {{ trans('db.backToCart') }}
                        </a>
                        <button type="submit" class="btn btn-gold pt-2 pb-2 ps-4 pe-4">
                            {{ trans('db.previewOrder') }} <i class="fa-solid fa-arrow-right"></i>
                        </button>
                    </div>
                </form>
        </div>

        @else
            <div class="text-center p-5">
                <div class="alert alert-danger">{!! trans('db.cartEmpty') !!}</div>
                <a href="/{{ $currentLocale }}" class="btn btn-gold pt-2 pb-2 ps-4 pe-4 mt-5">
                    {{ trans('db.startShopping') }}
                    <i class="fa-solid fa-arrow-right"></i>
                </a>
            </div>
        @endif

    </div>

@endsection



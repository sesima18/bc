@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => $currentCategory->name])

            @if(Cart::count() > 0)

                <div class="table-responsive cart-table">
                    <table class="table text-nowrap align-middle mb-0">
                        <thead>
                            <tr>
                                <th scope="col">{{ trans('db.product') }}</th>
                                <th scope="col"></th>
                                <th scope="col" class="text-center">{{ trans('db.price') }}</th>
                                <th scope="col" class="text-center">{{ trans('db.quantity') }}</th>
                                <th scope="col" class="text-end">{{ trans('db.subtotal') }}</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cartItems as $cartItem)
                                <tr data-cart-item="{{ $cartItem->id }}">
                                    <td style="width: 150px;">
                                        <a href="{{ $cartItem->link }}">
                                            <img src="{{ $cartItem->photo }}" alt="{{ $cartItem->name }}">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ $cartItem->link }}">
                                            {{ $cartItem->name }}
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        @if($cartItem->saleMoney)
                                            <del>{{ $cartItem->saleMoney->price }} </del> €
                                        @endif
                                        <span data-cart-product-price="{{ $cartItem->id }}">{{ $cartItem->priceMoney }}</span>
                                    </td>
                                    <td class="text-center">
                                        <div class="number-counter" id="number-counter">
                                            <button type="button" data-less-cart="{{ $cartItem->id }}">
                                                <i class="fa-solid fa-minus"></i>
                                            </button>
                                            <input type="text" min="1" max="10"
                                                   value="{{ $cartItem->quantity }}"
                                                   data-update-cart="{{ $cartItem->id }}" readonly />
                                            <button type="button" data-more-cart="{{ $cartItem->id }}">
                                                <i class="fa-solid fa-plus"></i>
                                            </button>
                                        </div>
                                    </td>
                                    <td class="text-end" data-cart-product-info="{{ $cartItem->id }}">{{ $cartItem->subtotal }}</td>
                                    <td class="p-0 text-end">
                                        <button type="button" data-remove-cart="{{ $cartItem->id }}">
                                            <i class="fa-regular fa-trash-can"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="7" class="text-end">{{ trans('db.total') }}: <span data-cart-info>{{ $cartTotal }}</span></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="d-flex justify-content-center mt-3">
                    <a href="{{ $orderCategory->link }}" class="btn btn-gold pt-2 pb-2 ps-4 pe-4">
                        {{ trans('db.order') }} <i class="fa-solid fa-arrow-right"></i>
                    </a>
                </div>

            @else
                <div class="text-center p-5">
                    <div class="alert alert-danger">{!! trans('db.cartEmpty') !!}</div>
                    <a href="/{{ $currentLocale }}" class="btn btn-gold mt-5 pt-2 pb-2 ps-4 pe-4">
                        {{ trans('db.startShopping') }}
                        <i class="fa-solid fa-arrow-right"></i>
                    </a>
                </div>
            @endif

        </div>
    </div>

@endsection



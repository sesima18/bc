@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => $currentCategory->name])

            @if(Cart::count() > 0)

                <div class="row">
                    <div class="col-4">
                        <h5 class="mb-15 mb-md-25 fw-bold text-black">{{ trans('db.orderInfo') }}</h5>
                        <ul class="ps-0 mb-0 list-unstyled order-preview">
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ __('validation.attributes.user_name') }}:</span> {{ $checkoutData->name }}
                            </li>
                            @if($checkoutData->surname)
                                <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                    <span>{{ __('validation.attributes.surname')  }}:</span> {{ $checkoutData->surname }}
                                </li>
                            @endif
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ __('validation.attributes.phone')  }}:</span> {{ $checkoutData->phone }}
                            </li>
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ __('validation.attributes.email')  }}:</span> {{ $checkoutData->email }}
                            </li>
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ __('validation.attributes.address')  }}:</span> {{ $checkoutData->address }}
                            </li>
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ __('validation.attributes.countryId')  }}:</span> {{ $checkoutData->country }}
                            </li>
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ trans('db.delivery') }}:</span> {{ $checkoutData->delivery }}
                            </li>
                            @if($checkoutData->deliveryTerminalName)
                                <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ trans('db.shippingTerminal') }}:</span> {{ $checkoutData->deliveryTerminalName }}
                                </li>
                            @endif
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ trans('db.payment') }}:</span> {{ $checkoutData->payment }}
                            </li>
                            <li class="text-paragraph fs-md-15 fs-lg-16 position-relative">
                                <span>{{ trans('db.message') }}:</span> {{ $checkoutData->comment }}
                            </li>
                        </ul>
                    </div>
                    <div class="col-8">
                        <h5 class="mb-15 mb-md-25 fw-bold text-black">{{ trans('db.products') }}</h5>
                        <div class="table-responsive cart-table">
                            <table class="table text-nowrap align-middle mb-0">
                                <thead>
                                <tr>
                                    <th scope="col">{{ trans('db.product') }}</th>
                                    <th scope="col"></th>
                                    <th scope="col" class="text-center">{{ trans('db.price') }}</th>
                                    <th scope="col" class="text-center">{{ trans('db.quantity') }}</th>
                                    <th scope="col" class="text-end">{{ trans('db.subtotal') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($checkoutData->cartItems as $cartItem)
                                    <tr>
                                        <td style="width: 150px;">
                                            <a href="{{ $cartItem->link }}">
                                                <img src="{{ $cartItem->photo }}" alt="{{ $cartItem->name }}">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ $cartItem->link }}">
                                                {{ $cartItem->name }}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            @if($cartItem->saleMoney)
                                                <del>{{ $cartItem->saleMoney->price }} </del> €
                                            @endif
                                            {{ $cartItem->priceMoney }}
                                        </td>
                                        <td class="text-center">{{ $cartItem->quantity }}</td>
                                        <td class="text-end">{{ $cartItem->subtotal }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="7" class="text-end">{{ trans('db.total') }}: <span data-cart-info>{{ $cartTotal }}</span></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-between mt-3">
                    <a href="{{ $orderCategory->link }}" class="link-gold">
                        <i class="fa-solid fa-arrow-left"></i> {{ trans('db.backToOrder') }}
                    </a>
                    <a href="{{ route('pay', $currentLocale) }}" class="btn btn-gold  pt-2 pb-2 ps-4 pe-4">
                        {{ trans('db.pay') }} <i class="fa-solid fa-arrow-right"></i>
                    </a>
                </div>

        </div>

        @else
            <div class="text-center p-5">
                <div class="alert alert-danger">{!! trans('db.cartEmpty') !!}</div>
                <a href="/{{ $currentLocale }}" class="btn btn-gold pt-2 pb-2 ps-4 pe-4 mt-5">
                    {{ trans('db.startShopping') }}
                    <i class="fa-solid fa-arrow-right"></i>
                </a>
            </div>
        @endif

    </div>


@endsection



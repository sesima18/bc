@extends('web.layouts.app')

@section('content')

    <div class="container">
        <div class="block-categories-slider ">
            @include('web.partials.title', ['title' => $currentCategory->name])

            <div class="alert alert-success">{{ trans('db.orderSuccess') }}</div>

    </div>


@endsection



<div class="block-categories-slider h-100">
    @include('web.partials.title', ['title' => trans('db.categories')])
    <ul class="list-group list-group-flush">
        @foreach($productsMenu as $menu)
            <li class="list-group-item">
                @if(count($menu->submenu) > 0)
                    <a class="menu-open d-flex justify-content-between align-items-center "
                       data-bs-toggle="collapse" href="#menu-{{ $menu->id }}" role="button" aria-expanded="false"
                       aria-controls="menu-{{ $menu->id }}">
                        <span>{{ $menu->name }}</span>
                        <i class="fa-solid fa-plus"></i>
                    </a>
                @else
                    <a class="{{ $menu->isInPath ? 'current' : '' }}" href="{{ $menu->link }}">{{ $menu->name }}
                        <small class="opacity-50">({{ $menu->productsTotal }})</small>
                    </a>
                @endif

                <div class="ms-4 collapse {{ $menu->isInPath ? 'show' : '' }}" id="menu-{{ $menu->id }}">
                    @foreach($menu->submenu as $menu2)
                        @if(count($menu2->submenu) > 0)
                            <a class="pt-2 level-2 menu-open "
                               data-bs-toggle="collapse" href="#menu-{{ $menu2->id }}" role="button"
                               aria-expanded="false" aria-controls="menu-{{ $menu2->id }}">
                                <div class=" d-flex justify-content-between align-items-center">
                                    <span>{{ $menu2->name }}</span>
                                    <i class="fa-solid fa-plus"></i>
                                </div>
                            </a>
                        @else
                            <a class="pt-2 level-2  {{ $menu2->isInPath ? 'current' : '' }}"
                               href="{{ $menu2->link }}">{{ $menu2->name }} <small
                                    class="opacity-50">({{ $menu2->productsTotal }})</small></a>
                        @endif

                        <div class="ms-4 collapse {{ $menu2->isInPath ? 'show' : '' }}" id="menu-{{ $menu2->id }}">
                            @foreach($menu2->submenu as $menu3)
                                <a class="level-3 pt-2 {{ $menu3->isInPath ? 'current' : '' }}"
                                   href="{{ $menu3->link }}">{{ $menu3->name }} <small
                                        class="opacity-50">({{ $menu3->productsTotal }})</small></a>
                            @endforeach
                        </div>

                    @endforeach
                </div>
            </li>
        @endforeach
    </ul>
</div>

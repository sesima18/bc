<div class="title mb-3">
    <span>
        @if(isset($link))
            <div onclick="window.location = '{{ $link }}'" class="title-gold cursor-pointer" data-heading="{{ $title }}" contenteditable>{{ $title }}</div>
        @else
            <div class="title-gold" data-heading="{{ $title }}" contenteditable>{{ $title }}</div>
        @endif
    </span>
</div>

@extends('web.layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 menu">
                @include('web.partials.productsMenu', ['title' => trans('db.categories')])
            </div>
            <div class="col-lg-9 col-md-8 col-sm-6 ">
                <div class="block-categories-slider">
                    @include('web.partials.title', ['title' => $topCategory->name, 'link' => $topCategory->link])
                    <div class="row">
                        @foreach($topProducts as $product)
                        <div class="col-lg-4 col-md-6 col-sm-12 product">
                            @include('web.products.product')
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="block-categories-slider">
            @include('web.partials.title', ['title' => $newCategory->name, 'link' => $newCategory->link])
            <div class="row">
                @foreach($newProducts as $product)
                    <div class="col-lg-3 col-md-4 col-sm-6 product">
                        @include('web.products.product')
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="container mt-5">
        <div class="block-categories-slider">
            @include('web.partials.title', ['title' => $bestCategory->name, 'link' => $bestCategory->link])
            <div class="row">
                @foreach($bestProducts as $product)
                    <div class="col-4 product">
                        @include('web.products.product')
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection



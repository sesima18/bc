import * as bootstrap from 'bootstrap'
import $ from "jquery";
import lightbox from 'lightbox2'

$('[data-add-to-cart]').click(function () {
    var info = $("[data-cart-info]");
    var url = $(this).attr("href");

    var timestamp = new Date().getTime();
    $.ajax(url + '?add=1t=' + timestamp).done(function (data) {
        $(info).text(data['total']);
    });

    var id = $(this).attr("data-add-to-cart");
    var n = $('.cart-img-' + id + ' img');
    if (n) var i = n.clone().offset({top: n.offset().top, left: n.offset().left}).css({
        opacity: "0.5",
        position: "absolute",
        height: "200px",
        width: "200px",
        "z-index": "100"
    }).appendTo($("body")).animate({
        top: info.offset().top + 10,
        left: info.offset().left + 10,
        width: 75,
        height: 75
    }, 1e3);
    i.animate({width: 0, height: 0}, function () {
        $(this).detach()
    })
    return false;
});

$('[data-update-cart]').keyup(function () {
    var id = $(this).attr("data-update-cart");
    var qty = $('[data-update-cart="' + id + '"]').val();
    updateCart(id, qty);
});

$('[data-less-cart]').click(function () {
    var id = $(this).attr("data-less-cart");
    var qty = Math.max(1, $('[data-update-cart="' + id + '"]').val() * 1 - 1);
    updateCart(id, qty);
});

$('[data-more-cart]').click(function () {
    var id = $(this).attr("data-more-cart");
    var qty = $('[data-update-cart="' + id + '"]').val() * 1 + 1;
    updateCart(id, qty);
});

$('[data-remove-cart]').click(function () {
    var id = $(this).attr("data-remove-cart");
    var qty = 0;
    updateCart(id, qty);
});

function updateCart(id, qty) {
    var url = '/cart/' + id + '/' + qty;
    var info = $("[data-cart-info]");
    var timestamp = new Date().getTime();
    $('[data-update-cart="' + id + '"]').val(qty);
    $.ajax(url + '?add=1t=' + timestamp).done(function (data) {
        $(info).text(data['total']);
        qty = data['quantity'];
        $('[data-update-cart="' + id + '"]').val(qty);
        $('.alert-cart').remove();
        if(data['error']) {
            $("<div class='alert alert-danger alert-cart'>" + data['error'] + "</div>").insertBefore(".cart-table");
        }
    });

    var subtotal = $('[data-cart-product-price="' + id + '"]').text().replace(' €', '') * 1 * qty;
    $('[data-cart-product-info="' + id + '"]').text(subtotal + ' €');

    if (qty == 0) {
        $('[data-cart-item="' + id + '"]').hide();
    }
}

$('[data-delivery-choose]').click(function () {
    var id = $(this).attr('data-delivery-choose');
    $('[data-delivery-choose] .dot').removeClass('dot-selected');
    $('[data-delivery-id]').val(id);
    $('[data-delivery-choose="' + id + '"] .dot').addClass('dot-selected');
});

$('[data-payment-choose]').click(function () {
    var id = $(this).attr('data-payment-choose');
    $('[data-payment-choose] .dot').removeClass('dot-selected');
    $('[data-payment-id]').val(id);
    $('[data-payment-choose="' + id + '"] .dot').addClass('dot-selected');
});

$('[data-thumb]').click(function () {
    var id = $(this).attr('data-thumb');
    $('.select-img-thumbnail').removeClass('select-img-thumbnail');
    $(this).addClass('select-img-thumbnail');
    $('[data-photo]').hide();
    $('[data-photo="' + id + '"]').show();
    return false;
});


$('select[name="countryId"]').change(function () {
    let country = $(this).val();
    $('[data-delivery-country]').hide();
    $('[data-delivery-country=""]').show();//all
    $('[data-delivery-country*="' + country + '"]').show();
});


$('[data-contact-from]').submit(function () {
    var form = $(this);
    $(form).find('.input-errors').removeClass('input-errors');
    $(form).find('.is-invalid').removeClass('is-invalid');
    $(form).find('small').remove();
    $('[data-contact-from-success]').hide();
    $.ajax({
        url: $(form).attr('action'),
        type: 'POST',
        data: $(form).serialize()
    }).done(function(response) {
        $('[data-contact-from-success]').show();
        $(form).find('.form-control').val('');
    }).fail(function(response) {
        $(response.responseJSON.errors).each(function( index, el ) {
            var nameField = $(form).find('input[name="name"]');
            var emailField = $(form).find('input[name="email"]');
            //var phoneField = $(form).find('input[name="phone"]');
            var messageField = $(form).find('textarea[name="message"]');
            if (el.name != undefined) {
                nameField.addClass('is-invalid');
                nameField.parent().addClass('input-errors').append('<small class="invalid-feedback">' + el.name + '</small>');
            }
            if (el.email != undefined) {
                emailField.addClass('is-invalid');
                emailField.parent().addClass('input-errors').append('<small class="invalid-feedback">' + el.email + '</small>');
            }
            // if (el.phone != undefined) {
            //     phoneField.parent().addClass('input-errors').append('<small>' + el.phone + '</small>');
            // }
            if (el.message != undefined) {
                messageField.addClass('is-invalid');
                messageField.parent().addClass('input-errors').append('<small class="invalid-feedback">' + el.message + '</small>');
            }
        });
    });
    return false;
});

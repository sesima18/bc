import authRepository from "../../repositories/authRepository.js";
import router from '../../router/index.ts';
import {ROUTE_DASHBOARD, ROUTE_LOGIN} from "../../const/routes.js";

export const AUTH_TOKEN_KEY = 'auth-token';
export const USER_KEY = 'user';
export const PERMISSIONS_KEY = 'permissions';

export default {
    login(formData) {
        return authRepository.login(formData).then((response) => {
            localStorage.setItem(AUTH_TOKEN_KEY, response.token);
            localStorage.setItem(USER_KEY, JSON.stringify(response.user));

            window.location.href = ROUTE_DASHBOARD;

            return response;
        }).catch((response) => {
            return {errors: response};
        })
    },
    logout() {
        authRepository.logout();
        localStorage.removeItem(AUTH_TOKEN_KEY);
        localStorage.removeItem(USER_KEY);
        localStorage.removeItem(PERMISSIONS_KEY);
        router.push({name: ROUTE_LOGIN});
    },
    getUser() {
        return JSON.parse(localStorage.getItem(USER_KEY));
    }
}

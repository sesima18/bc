import Repository from '../../repositories/Repository.js';

const resource = 'languages';

export default {

    index() {
        return Repository.get(`${resource}`).then(response => response);
    }
}

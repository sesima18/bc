import Repository from '../../repositories/Repository.js';

const resource = 'products';

export default {

    index(perPage, page, query) {
        return Repository.get(`${resource}?perPage=${perPage}&page=${page}&query=${query}`).then(response => response);
    },
    find(id) {
        return Repository.get(`${resource}/${id}`);
    },
    store(formData) {
        return Repository.post(`${resource}`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            return response;
        }).catch((response) => {
            return {errors: response};
        })
    },
    patch(id, formData) {
        formData['_method'] = 'patch';
        return Repository.post(`${resource}/${id}`, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            return response;
        }).catch((response) => {
            return {errors: response};
        })
    },
    delete(id) {
        return Repository.delete(`${resource}/${id}`).then((response) => {
            return response;
        });
    },
    priority(formData) {
        return Repository.post(`${resource}/change-priority`, formData).then((response) => {
            return response;
        });
    }
}

import PermissionsRepository from "../../repositories/PermissionsRepository.js";

export default {
    getUserPermissions() {
        return PermissionsRepository.getUserPermissions();
    }
}

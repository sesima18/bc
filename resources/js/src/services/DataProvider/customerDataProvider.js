import Repository from '../../repositories/Repository.js';

const resource = 'customers';

export default {

    index(perPage, page, query) {
        return Repository.get(`${resource}?perPage=${perPage}&page=${page}&query=${query}`).then(response => response);
    },
    find(id) {
        return Repository.get(`${resource}/${id}`);
    },
    store(formData) {
        return Repository.post(`${resource}`, formData).then((response) => {
            return response;
        }).catch((response) => {
            return {errors: response};
        })
    },
    patch(id, formData) {
        return Repository.patch(`${resource}/${id}`, formData).then((response) => {
            return response;
        }).catch((response) => {
            return {errors: response};
        })
    },
    delete(id) {
        return Repository.delete(`${resource}/${id}`).then((response) => {
            return response;
        });
    }
}

import Repository from '../../repositories/Repository.js';

const resource = 'categories';

export default {

    index() {
        return Repository.get(`${resource}`).then(response => response);
    },
    find(id) {
        return Repository.get(`${resource}/${id}`);
    },
    store(formData) {
        return Repository.post(`${resource}`, formData).then((response) => {
            return response;
        }).catch((response) => {
            return {errors: response};
        })
    },
    patch(id, formData) {
        return Repository.patch(`${resource}/${id}`, formData).then((response) => {
            return response;
        }).catch((response) => {
            return {errors: response};
        })
    },
    delete(id) {
        return Repository.delete(`${resource}/${id}`).then((response) => {
            return response;
        });
    },
    priority(formData) {
        return Repository.post(`${resource}/change-priority`, formData).then((response) => {
            return response;
        });
    }
}

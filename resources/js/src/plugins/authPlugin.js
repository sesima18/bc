import store from "../state/store.ts";


const fetchPermissions = async () => {
    if (window.location.pathname === '/admin17/login') { //todo
        return [];
    }

    // Assuming FETCH_PERMISSIONS returns a promise
    return await store.dispatch('FETCH_PERMISSIONS');
};

export default {
    install: async (app) => {
        const permissions = await fetchPermissions();

        app.config.globalProperties.$can = (permission) => {
            return permissions.includes(permission);
        };

        store.watch(
            (state) => state.permissions,
            (newPermissions) => {
                permissions.splice(0, permissions.length, ...newPermissions);
            }
        );
    },
};

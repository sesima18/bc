import PermissionsDataProvider from "../../services/DataProvider/PermissionsDataProvider.js";

const state = {
    permissions: [],
    permissionsPromise: null,
};

const mutations = {
    SET_PERMISSIONS(state, permissions) {
        state.permissions = permissions;
    },
    SET_PERMISSIONS_PROMISE(state, promise) {
        state.permissionsPromise = promise;
    },
}

const actions = {
    FETCH_PERMISSIONS({commit, state}) {
        if (null === state.permissionsPromise && 0 === state.permissions.length) {
            const promise = PermissionsDataProvider.getUserPermissions().then(response => {
                const permissions = response.data.map(permission => permission.key);
                commit('SET_PERMISSIONS', permissions);
                commit('SET_PERMISSIONS_PROMISE', null);

                return permissions;
            }).catch((response) => {
                return [];
            })
            commit('SET_PERMISSIONS_PROMISE', promise);

            return promise;
        }
        if (null === state.permissionsPromise && state.permissions.length > 0) {
            return state.permissions;
        }

        return state.permissionsPromise;
    }
}

export default {
    state: state,
    mutations: mutations,
    actions: actions,
}

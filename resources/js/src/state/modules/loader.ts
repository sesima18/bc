const state = {
    loader: 0,
};

const mutations = {
    INCREMENT_LOADER(state) {
        state.loader = ++state.loader;
    },
    DECREMENT_LOADER(state) {
        state.loader = --state.loader;
    },
    CLEAR_LOADER(state) {
        state.loader = 0;
    }
}

const actions = {
    startLoader({commit}) {
        commit('INCREMENT_LOADER');
    },
    stopLoader({commit}) {
        commit('DECREMENT_LOADER');
    },
    clearLoader({commit}) {
        commit('CLEAR_LOADER');
    }
}

export default {
    state: state,
    mutations: mutations,
    actions: actions,
}

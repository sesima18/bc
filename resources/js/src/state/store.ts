import {createStore} from 'vuex';
import loader from './modules/loader.ts';
import permissions from './modules/permissions.ts';

const store = createStore({
        modules: {
            loader,
            permissions
        },
        strict: true,
    })
;

export default store;

import {createWebHistory, createRouter} from "vue-router";

import {
    ROUTE_DASHBOARD, ROUTE_LOGIN,
    ROUTE_USERS_LIST, ROUTE_USERS_CREATE, ROUTE_USERS_EDIT,
    ROUTE_ROLES_LIST, ROUTE_ROLES_CREATE, ROUTE_ROLES_EDIT,
    ROUTE_CATEGORIES_LIST, ROUTE_CATEGORIES_CREATE, ROUTE_CATEGORIES_EDIT,
    ROUTE_PRODUCTS_LIST, ROUTE_PRODUCTS_CREATE, ROUTE_PRODUCTS_EDIT,
    ROUTE_SETTINGS_LIST, ROUTE_SETTINGS_CREATE, ROUTE_SETTINGS_EDIT,
    ROUTE_ORDERS_LIST, ROUTE_ORDERS_EDIT,
    ROUTE_CUSTOMERS_LIST, ROUTE_CUSTOMERS_EDIT
} from "../const/routes.js";
import {
    LIST_USERS, CREATE_USER, EDIT_USER,
    LIST_ROLES, CREATE_ROLE, EDIT_ROLE,
    LIST_CATEGORIES, CREATE_CATEGORY, EDIT_CATEGORY,
    LIST_PRODUCTS, CREATE_PRODUCT, EDIT_PRODUCT,
    LIST_SETTINGS, CREATE_SETTING, EDIT_SETTING,
    LIST_ORDERS, EDIT_ORDER,
    LIST_CUSTOMERS, EDIT_CUSTOMER
} from "../const/permissions.js";
import authDataProvider from '../services/DataProvider/authDataProvider.js';
import store from "../state/store.ts";

const adminPath = '/admin17';

const routes = [
    {
        path: adminPath + '/',
        name: ROUTE_DASHBOARD,
        component: () => import('../pages/Dashboard.vue'),
        meta: {
            authorize: true,
        }
    },
    {
        path: adminPath + "/login",
        name: ROUTE_LOGIN,
        component: () => import('../pages/Authentication/Login.vue'),
        meta: {
            authorize: false,
        }
    },

    {
        path: adminPath + "/users/:page?",
        name: ROUTE_USERS_LIST,
        component: () => import('../pages/Users/list.vue'),
        meta: {
            permission: LIST_USERS,
        }
    },
    {
        path: adminPath + "/users/create",
        name: ROUTE_USERS_CREATE,
        component: () => import('../pages/Users/create.vue'),
        meta: {
             permission: CREATE_USER,
        }
    },
    {
        path: adminPath + "/user/edit/:id",
        name: ROUTE_USERS_EDIT,
        component: () => import('../pages/Users/edit.vue'),
        meta: {
              permission: EDIT_USER,
        }
    },

    {
        path: adminPath + "/roles",
        name: ROUTE_ROLES_LIST,
        component: () => import('../pages/Roles/list.vue'),
        meta: {
            permission: LIST_ROLES,
        }
    },
    {
        path: adminPath + "/roles/create",
        name: ROUTE_ROLES_CREATE,
        component: () => import('../pages/Roles/create.vue'),
        meta: {
             permission: CREATE_ROLE,
        }
    },
    {
        path: adminPath + "/roles/edit/:id",
        name: ROUTE_ROLES_EDIT,
        component: () => import('../pages/Roles/edit.vue'),
        meta: {
              permission: EDIT_ROLE,
        }
    },

    {
        path: adminPath + "/categories",
        name: ROUTE_CATEGORIES_LIST,
        component: () => import('../pages/Categories/list.vue'),
        meta: {
           permission: LIST_CATEGORIES,
        }
    },
    {
        path: adminPath + "/categories/create",
        name: ROUTE_CATEGORIES_CREATE,
        component: () => import('../pages/Categories/create.vue'),
        meta: {
           permission: CREATE_CATEGORY,
        }
    },
    {
        path: adminPath + "/categories/edit/:id",
        name: ROUTE_CATEGORIES_EDIT,
        component: () => import('../pages/Categories/edit.vue'),
        meta: {
           permission: EDIT_CATEGORY,
        }
    },

    {
        path: adminPath + "/products/:page?/:search?",
        name: ROUTE_PRODUCTS_LIST,
        component: () => import('../pages/Products/list.vue'),
        meta: {
            permission: LIST_PRODUCTS,
        }
    },
    {
        path: adminPath + "/products/create",
        name: ROUTE_PRODUCTS_CREATE,
        component: () => import('../pages/Products/create.vue'),
        meta: {
            permission: CREATE_PRODUCT,
        }
    },
    {
        path: adminPath + "/products/edit/:id",
        name: ROUTE_PRODUCTS_EDIT,
        component: () => import('../pages/Products/edit.vue'),
        meta: {
            permission: EDIT_PRODUCT,
        }
    },


    {
        path: adminPath + "/settings/:page?/:search?",
        name: ROUTE_SETTINGS_LIST,
        component: () => import('../pages/Settings/list.vue'),
        meta: {
            permission: LIST_SETTINGS,
        }
    },
    {
        path: adminPath + "/settings/create",
        name: ROUTE_SETTINGS_CREATE,
        component: () => import('../pages/Settings/create.vue'),
        meta: {
            permission: CREATE_SETTING,
        }
    },
    {
        path: adminPath + "/settings/edit/:id",
        name: ROUTE_SETTINGS_EDIT,
        component: () => import('../pages/Settings/edit.vue'),
        meta: {
            permission: EDIT_SETTING,
        }
    },

    {
        path: adminPath + "/orders/:page?/:search?",
        name: ROUTE_ORDERS_LIST,
        component: () => import('../pages/Orders/list.vue'),
        meta: {
            permission: LIST_ORDERS,
        }
    },
    {
        path: adminPath + "/orders/edit/:id",
        name: ROUTE_ORDERS_EDIT,
        component: () => import('../pages/Orders/edit.vue'),
        meta: {
            permission: EDIT_ORDER,
        }
    },

    {
        path: adminPath + "/customers/:page?/:search?",
        name: ROUTE_CUSTOMERS_LIST,
        component: () => import('../pages/Customers/list.vue'),
        meta: {
            permission: LIST_CUSTOMERS,
        }
    },
    {
        path: adminPath + "/customers/edit/:id",
        name: ROUTE_CUSTOMERS_EDIT,
        component: () => import('../pages/Customers/edit.vue'),
        meta: {
            permission: EDIT_CUSTOMER,
        }
    },

    {
        path: adminPath + "/:pathMatch(.*)*",
        name: "ErrorPage",
        component: () => import('../pages/ErrorPage.vue'),
    },
];

const router = createRouter({
    history: createWebHistory(),
    linkExactActiveClass: "active",
    routes: routes,
});

const fetchPermissions = async () => {
    const routeLogin = router.resolve({name: ROUTE_LOGIN}).href;
    if (window.location.pathname === routeLogin) {
        return [];
    }

    return await store.dispatch('FETCH_PERMISSIONS');
};
router.beforeEach((to, from, next) => {
    const user = authDataProvider.getUser();

    if (ROUTE_LOGIN === to.name && null !== user) {
        return next(router.resolve({name: ROUTE_DASHBOARD}).href);
    }

    if (ROUTE_LOGIN === to.name) {
        return next();
    }

    if ((to.meta?.authorize || (to.meta?.permission && to.meta?.permission.length)) && null === user) {
        return next(router.resolve({name: ROUTE_LOGIN}).href);
    }

    if (to.meta?.authorize && null !== user) {
        return next();
    }

    return fetchPermissions().then(permissions => {
        const hasPermission = permissions.includes(to.meta?.permission);

        if (hasPermission) {
            return next();
        }

        return next(router.resolve({name: ROUTE_LOGIN}).href);
    })
})
export default router;

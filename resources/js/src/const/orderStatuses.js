export const STATUS_NEW = {1: 'Naujas'};
export const STATUS_PAID = {2: 'Apmokėtas'};
export const STATUS_SEND = {3: 'Išsiųstas'};
export const STATUS_CANCELLED = {4: 'Atšauktas'};
export const STATUS_ENDED = {5: 'Užbaigtas'};
export const STATUSES_LIST = [
    STATUS_NEW, STATUS_PAID, STATUS_SEND, STATUS_CANCELLED, STATUS_ENDED
];

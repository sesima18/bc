export const ROUTE_DASHBOARD = 'dashboard';
export const ROUTE_LOGIN = 'login';

export const ROUTE_USERS_LIST = 'users';
export const ROUTE_USERS_CREATE = `${ROUTE_USERS_LIST}_create`;
export const ROUTE_USERS_EDIT = `${ROUTE_USERS_LIST}_edit`;

export const ROUTE_ROLES_LIST = 'roles';
export const ROUTE_ROLES_CREATE = `${ROUTE_ROLES_LIST}_create`;
export const ROUTE_ROLES_EDIT = `${ROUTE_ROLES_LIST}_edit`;

export const ROUTE_CATEGORIES_LIST = 'categories';
export const ROUTE_CATEGORIES_CREATE = `${ROUTE_CATEGORIES_LIST}_create`;
export const ROUTE_CATEGORIES_EDIT = `${ROUTE_CATEGORIES_LIST}_edit`;

export const ROUTE_PRODUCTS_LIST = 'products';
export const ROUTE_PRODUCTS_CREATE = `${ROUTE_PRODUCTS_LIST}_create`;
export const ROUTE_PRODUCTS_EDIT = `${ROUTE_PRODUCTS_LIST}_edit`;

export const ROUTE_SETTINGS_LIST = 'settings';
export const ROUTE_SETTINGS_CREATE = `${ROUTE_SETTINGS_LIST}_create`;
export const ROUTE_SETTINGS_EDIT = `${ROUTE_SETTINGS_LIST}_edit`;

export const ROUTE_ORDERS_LIST = 'orders';
export const ROUTE_ORDERS_EDIT = `${ROUTE_ORDERS_LIST}_edit`;

export const ROUTE_CUSTOMERS_LIST = 'customers';
export const ROUTE_CUSTOMERS_EDIT = `${ROUTE_CUSTOMERS_LIST}_edit`;

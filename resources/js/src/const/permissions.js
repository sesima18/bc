export const LIST_USERS = 'list_users';
export const CREATE_USER = 'create_user';
export const DELETE_USER = 'delete_user';
export const EDIT_USER = 'edit_user';

export const LIST_ROLES = 'list_roles';
export const CREATE_ROLE = 'create_role';
export const DELETE_ROLE = 'delete_role';
export const EDIT_ROLE = 'edit_role';

export const LIST_CATEGORIES = 'list_categories';
export const CREATE_CATEGORY = 'create_category';
export const DELETE_CATEGORY = 'delete_category';
export const DELETE_CATEGORY_UN_DELETABLE = 'delete_category_un_deletable';
export const EDIT_CATEGORY = 'edit_category';

export const LIST_PRODUCTS = 'list_products';
export const CREATE_PRODUCT = 'create_product';
export const DELETE_PRODUCT = 'delete_product';
export const EDIT_PRODUCT = 'edit_product';

export const LIST_SETTINGS = 'list_settings';
export const CREATE_SETTING = 'create_setting';
export const DELETE_SETTING = 'delete_setting';
export const EDIT_SETTING = 'edit_setting';

export const LIST_ORDERS = 'list_orders';
export const DELETE_ORDER = 'delete_order';
export const EDIT_ORDER = 'edit_order';

export const LIST_CUSTOMERS = 'list_customers';
export const DELETE_CUSTOMER = 'delete_customer';
export const EDIT_CUSTOMER = 'edit_customer';

export const LIST = [
    LIST_USERS, CREATE_USER, DELETE_USER, EDIT_USER,
    LIST_ROLES, CREATE_ROLE, DELETE_ROLE, EDIT_ROLE,
    LIST_CATEGORIES, CREATE_CATEGORY, DELETE_CATEGORY, EDIT_CATEGORY, DELETE_CATEGORY_UN_DELETABLE,
    LIST_PRODUCTS, CREATE_PRODUCT, DELETE_PRODUCT, EDIT_PRODUCT,
    LIST_SETTINGS, CREATE_SETTING, DELETE_SETTING, EDIT_SETTING,
    LIST_ORDERS, DELETE_ORDER, EDIT_ORDER,
    LIST_CUSTOMERS, DELETE_CUSTOMER, EDIT_CUSTOMER
];

import axios from 'axios';

const BASE_URL = '/api';

function authHeader() {
    let token = localStorage.getItem('auth-token');
    if (!token) {
        return {};
    }

    return {Authorization: `Bearer ${token}`}
}

function handleErrorResponse(response) {
    if (!response.status || response.status < 200 || response.status >= 300) {
        return Promise.reject((response.data && response.data.errors) || response.data.message);
    }

    return response.data;
}

const instance = axios.create({
    baseURL: BASE_URL,
    headers: authHeader(),
})

instance.interceptors.response.use(
    (response) => handleErrorResponse(response),
    (error) => handleErrorResponse(error.response),
);

export default instance;


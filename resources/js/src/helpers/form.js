export default {

    fillParam(form, param, copyFromParam) {
        Object.fromEntries(Object.entries(form).map(
            ([k, v]) => [k, v[param] = (v[param] == null || v[param] === '') ? v[copyFromParam] : v[param]])
        );
    },

    makeSlug(str) {
        str = str.replace(/^\s+|\s+$/g, '');
        str = str.toLowerCase();
        str = str.replace(/ą|Ą/g, 'a')
        str = str.replace(/č|Č/g, 'c')
        str = str.replace(/ę|Ę|ė|Ė/g, 'e')
        str = str.replace(/į|Į/g, 'i')
        str = str.replace(/š|Š/g, 's')
        str = str.replace(/ų|Ų|ū|Ū/g, 'u')
        str = str.replace(/ž|Ž/g, 'z')
        str = str.replace(/[^a-z0-9 -]/g, '')
            .replace(/\s+/g, '-')
            .replace(/-+/g, '-');
    }

}

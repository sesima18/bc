import Repository from './Repository';

const resource = 'permissions';

export default {
    getUserPermissions() {
        return Repository.get(`/user/${resource}`);
    },
}

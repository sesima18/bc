import axios from '../helpers/axios.js';

export default {
    get(resource) {
        return axios.get(resource);
    },
    post(resource, data, config = {}) {
        return axios.post(resource, data, config);
    },
    patch(resource, data) {
        return axios.patch(resource, data);
    },
    delete(resource, data) {
        return axios.delete(resource);
    },
}

import Repository from './Repository';

const loginResource = '/login';
const logoutResource = '/logout';

export default {
    login(formData) {
        return Repository.post(`${loginResource}`, formData);
    },
    logout() {
        return Repository.get(`${logoutResource}`);
    }
}

<?php

use App\Http\Controllers\Web\CheckoutController;
use App\Http\Controllers\Web\CustomerController;
use App\Http\Controllers\Web\IndexController;
use Illuminate\Support\Facades\Route;

Route::get('/admin17/{any?}', function () {
    return view('admin');
})->where('any', '.*');

Route::get('/cart/{id}/{qty}', [CheckoutController::class, 'add'])->name('add');
Route::post('/{locale}/checkout', [CheckoutController::class, 'checkout'])->name('checkout');
Route::get('/{locale}/pay', [CheckoutController::class, 'pay'])->name('pay');

Route::post('/{locale}/login', [CustomerController::class, 'login'])->name('login');
Route::get('/{locale}/logout', [CustomerController::class, 'logout'])->name('logout');
Route::post('/{locale}/register', [CustomerController::class, 'register'])->name('register');
Route::post('/{locale}/profile', [CustomerController::class, 'profile'])->name('profile');
Route::post('/{locale}/forgot-password', [CustomerController::class, 'forgotPassword'])->name('forgotPassword');
Route::post('/{locale}/rest-password', [CustomerController::class, 'resetPassword'])->name('resetPassword');
Route::get('/reset-password/{token}', [IndexController::class, 'resetPassword'])->name('password.reset');

Route::post('/{locale}/contacts', [IndexController::class, 'contacts'])->name('contacts');

Route::get('/{any?}', [IndexController::class, 'index'])->where('any', '.*');

<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\LanguageController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\UsersController;
use Illuminate\Support\Facades\Route;

Route::post('/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::apiResource('users', UsersController::class);
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::get('/user/permissions', [AuthController::class, 'permissions']);
    Route::apiResource('roles', RoleController::class);
    Route::apiResource('languages', LanguageController::class)->only('index');

    Route::apiResource('categories', CategoryController::class);
    Route::post('/categories/change-priority', [CategoryController::class, 'changePriority']);

    Route::apiResource('products', ProductController::class);
    Route::get('/settings/tabs', [SettingController::class, 'tabs']);
    Route::get('/settings/findByKey/{key}', [SettingController::class, 'findByKey']);
    Route::apiResource('settings', SettingController::class);

    Route::apiResource('orders', OrderController::class)->except(['store']);
    Route::apiResource('customers', CustomerController::class)->except(['store']);
});

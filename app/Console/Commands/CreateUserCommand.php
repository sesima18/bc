<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class CreateUserCommand extends Command
{
    protected $signature = 'app:create-user {name} {email} {password}';
    protected $description = 'Creates new user.';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $name = $this->argument('name');
        $email = $this->argument('email');
        $password = bcrypt($this->argument('password'));

        User::create(
            [
                'name' => $name,
                'email' => $email,
                'password' => $password,
                'role_id' => 1,
            ]
        );
        $this->info('User created successfully.');
    }
}

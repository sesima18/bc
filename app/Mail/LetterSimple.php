<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Queue\SerializesModels;

class LetterSimple extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * LetterSimple constructor.
     * @param string $subject
     * @param string $viewName
     * @param array $viewData
     *
     * @return void
     */
    public function __construct($subject, $viewName, array $viewData)
    {
        $this->subject = $subject;
        $this->viewName = $viewName;
        $this->viewData = $viewData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->viewName)->with($this->viewData)->subject($this->subject);
    }
}

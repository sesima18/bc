<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Checkbox extends Component
{

    public string $name;
    public ?string $value = null;
    public ?string $label = null;
    public ?string $id = null;
    public ?bool $checked = false;

    public function __construct(string $name, ?string $value = null, ?string $label = null, ?string $id = null, ?bool $checked = false)
    {
        $this->name = $name;
        $this->value = old($name, $value);
        $this->id = $id;
        $this->label = $label;
        $this->checked = $checked;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.forms.checkbox');
    }
}

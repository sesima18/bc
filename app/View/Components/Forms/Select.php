<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Select extends Component
{
    public string $name;
    public ?string $value = null;
    public ?string $placeholder = null;
    public ?string $label = null;
    public array $options = [];
    /**
     * Create a new component instance.
     */
    public function __construct(string $name, ?string $value = null, ?string $placeholder = null, ?string $label = null, array $options = [])
    {
        $this->name = $name;
        $this->value = old($name, $value);
        $this->label = $label;
        $this->placeholder = $placeholder;
        $this->options = $options;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.forms.select');
    }
}

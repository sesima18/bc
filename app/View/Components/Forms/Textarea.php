<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Textarea extends Component
{
    public string $name;
    public ?string $value = null;
    public ?string $placeholder = null;
    public ?string $label = null;
    /**
     * Create a new component instance.
     */
    public function __construct(string $name, ?string $value = null, ?string $placeholder = null, ?string $label = null)
    {
        $this->name = $name;
        $this->value = old($name, $value);
        $this->placeholder = $placeholder;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.forms.textarea');
    }
}

<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;

class CountryDescription extends Model
{
    protected $table = 'countries_descriptions';

    protected $fillable = ['language_id', 'name'];
}

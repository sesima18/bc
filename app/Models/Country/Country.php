<?php

namespace App\Models\Country;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Country extends Model
{
    protected $fillable = ['code'];

    public function descriptions(): HasMany
    {
        return $this->hasMany(CountryDescription::class);
    }
}

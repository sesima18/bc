<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CategoryDescription extends Model
{
    protected $table = 'categories_descriptions';

    protected $fillable = ['language_id', 'name', 'slug', 'body'];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}

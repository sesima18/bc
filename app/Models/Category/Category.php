<?php

namespace App\Models\Category;

use App\Models\Product\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    const MAX_LEVEL = 3;

    protected $fillable = ['parent_id', 'hidden', 'priority', 'menu_header', 'menu_footer_1', 'menu_footer_2'];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function descriptions(): HasMany
    {
        return $this->hasMany(CategoryDescription::class);
    }

    protected function getParentsListAttribute(): array
    {
        $items = [];
        $level = 0;
        $parent = $this->parent;
        while ($level < Category::MAX_LEVEL && $parent) {
            if($parent) {
                $items[] = $parent->id;
                $parent = $parent->parent;
            }
            $level++;
        }
        return $items;
    }

    public function products(): HasMany
    {
        return $this->hasMany(ProductCategory::class);
    }
}

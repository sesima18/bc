<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SettingDescription extends Model
{
    protected $table = 'settings_descriptions';

    protected $fillable = ['language_id', 'body'];

    public function setting(): BelongsTo
    {
        return $this->belongsTo(Setting::class);
    }
}

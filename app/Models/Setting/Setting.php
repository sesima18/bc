<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Setting extends Model
{
    protected $fillable = ['key',  'tab_id', 'name'];

    public function descriptions(): HasMany
    {
        return $this->hasMany(SettingDescription::class);
    }

    public function tab(): BelongsTo
    {
        return $this->belongsTo(SettingTab::class);
    }
}

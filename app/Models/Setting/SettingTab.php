<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;

class SettingTab extends Model
{
    protected $table = 'settings_tabs';

    protected $fillable = ['name'];
}

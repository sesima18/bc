<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductDescription extends Model
{
    protected $table = 'products_descriptions';

    protected $fillable = ['language_id', 'name', 'slug', 'body'];

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }
}

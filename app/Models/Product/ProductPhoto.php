<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ProductPhoto extends Model
{
    protected $table = 'products_photos';

    protected $fillable = [ 'product_id', 'name', 'priority'];

    const IMAGES_PATH = '/images/products/';

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class);
    }

    public function getPhotoUrlAttribute(): string
    {
        return self::IMAGES_PATH.$this->name;
    }
}

<?php

namespace App\Models\Product;

use App\Enums\OrderStatuses;
use App\Models\Order\OrderCartItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    protected $fillable = ['price', 'sale', 'quantity', 'hidden', 'priority', 'top'];

    public function descriptions(): HasMany
    {
        return $this->hasMany(ProductDescription::class);
    }

    public function categories(): HasMany
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function photos(): HasMany
    {
        return $this->hasMany(ProductPhoto::class);
    }

    public function cartsItems(): HasMany
    {
        return $this->hasMany(OrderCartItem::class, 'product_id', 'id');
    }

    public function getQuantityWebAttribute(): int
    {
        $sold = $this->cartsItems()->whereHas(
            'order', fn($q) => $q->where('status', '!=', OrderStatuses::STATUS_CANCELLED)
        )->sum('quantity');
        return $this->quantity - $sold;
    }

    public function getOrdersAttribute() {
        return $this->cartsItems()->with('order')->get();
    }
}

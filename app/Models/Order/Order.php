<?php

namespace App\Models\Order;

use App\Enums\OrderStatuses;
use App\Models\Country\Country;
use App\Models\Customer\Customer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use Notifiable;

    protected $fillable = [
        'customer_id',
        'name',
        'surname',
        'email',
        'phone',
        'address',
        'comment',
        'country_id',
        'delivery_id',
        'delivery',
        'delivery_terminal_code',
        'delivery_terminal_name',
        'payment_id',
        'payment',
        'total',
        'status',
        'track_code',
        'language_id'
    ];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function cartItems(): HasMany
    {
        return $this->hasMany(OrderCartItem::class);
    }

    public function getStatusNameAttribute()
    {
        $orderStatuses = [
            '',
            OrderStatuses::STATUS_NEW => trans('db.orderStatusNew'),
            OrderStatuses::STATUS_PAID => trans('db.orderStatusPaid'),
            OrderStatuses::STATUS_SEND => trans('db.orderStatusSend'),
            OrderStatuses::STATUS_CANCELLED => trans('db.orderStatusCancelled'),
            OrderStatuses::STATUS_ENDED => trans('db.orderStatusEnded')
        ];
        return $orderStatuses[$this->status];
    }
}

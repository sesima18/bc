<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;

class PaymentDescription extends Model
{
    protected $table = 'payments_descriptions';

    protected $fillable = ['language_id', 'name'];
}

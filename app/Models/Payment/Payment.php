<?php

namespace App\Models\Payment;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Payment extends Model
{
    public function descriptions(): HasMany
    {
        return $this->hasMany(PaymentDescription::class);
    }
}

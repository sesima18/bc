<?php

namespace App\Models\Delivery;

use Illuminate\Database\Eloquent\Model;

class DeliveryDescription extends Model
{
    protected $table = 'deliveries_descriptions';

    protected $fillable = ['language_id', 'name'];
}

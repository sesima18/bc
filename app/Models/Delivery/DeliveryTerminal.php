<?php

namespace App\Models\Delivery;

use Illuminate\Database\Eloquent\Model;

class DeliveryTerminal extends Model
{
    protected $table = 'deliveries_terminals';

    protected $fillable = ['delivery_id', 'code', 'name', 'address', 'city', 'postal_code', 'country'];
}

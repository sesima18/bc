<?php

namespace App\Models\Delivery;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Delivery extends Model
{
    public function descriptions(): HasMany
    {
        return $this->hasMany(DeliveryDescription::class);
    }

    public function terminals(): HasMany
    {
        return $this->hasMany(DeliveryTerminal::class);
    }

    public function getCitiesAttribute()
    {
        return $this->terminals()->where('country', 'LT')->groupBy('city')->orderBy('city')->pluck('city')->toArray();
    }
}

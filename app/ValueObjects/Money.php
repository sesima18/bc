<?php

namespace App\ValueObjects;

final readonly class Money
{
    public string $price;
    public function __construct(
        public int $amount,
        public ?string $currency = '€'
    ) {
        $this->price = number_format($amount, 2, '.', '');
    }

    public function currency(): string
    {
        return $this->currency;
    }

    public function amount(): int
    {
        return $this->amount;
    }

    public function __toString(): string
    {
        $amount = number_format($this->price, 2, '.', '');
        $currency = ($this->currency()) ? ' '.$this->currency():'';
        return "{$amount}{$currency}";
    }
}

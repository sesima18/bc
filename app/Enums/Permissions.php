<?php

namespace App\Enums;

enum Permissions: string
{
    case LIST_USERS = 'list_users';
    case CREATE_USER = 'create_user';
    case DELETE_USER = 'delete_user';
    case EDIT_USER = 'edit_user';

    case LIST_ROLES = 'list_roles';
    case CREATE_ROLE = 'create_role';
    case DELETE_ROLE = 'delete_role';
    case EDIT_ROLE = 'edit_role';

    case LIST_CATEGORIES = 'list_categories';
    case CREATE_CATEGORY = 'create_category';
    case DELETE_CATEGORY = 'delete_category';
    case DELETE_CATEGORY_UN_DELETABLE = 'delete_category_un_deletable';
    case EDIT_CATEGORY = 'edit_category';

    case LIST_PRODUCTS = 'list_products';
    case CREATE_PRODUCT = 'create_product';
    case DELETE_PRODUCT = 'delete_product';
    case EDIT_PRODUCT = 'edit_product';

    case LIST_SETTINGS = 'list_settings';
    case CREATE_SETTING = 'create_setting';
    case DELETE_SETTING = 'delete_setting';
    case EDIT_SETTING = 'edit_setting';

    case LIST_ORDERS = 'list_orders';
    case DELETE_ORDER = 'delete_order';
    case EDIT_ORDER = 'edit_order';

    case LIST_CUSTOMERS = 'list_customers';
    case DELETE_CUSTOMER = 'delete_customer';
    case EDIT_CUSTOMER = 'edit_customer';
}

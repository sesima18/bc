<?php

namespace App\Enums;

enum OrderStatuses: string
{
    const STATUS_NEW = 1;
    const STATUS_PAID = 2;
    const STATUS_SEND = 3;
    const STATUS_CANCELLED = 4;
    const STATUS_ENDED = 5;
}

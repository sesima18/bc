<?php

namespace App\Enums;

enum CategoryTypes: string
{
    case HOME = 'home';
    case PRODUCTS = 'product';
    case CART = 'cart';
    case CHECKOUT = 'checkout';
    case CUSTOMER = 'customer';
    case SEARCH = 'search';
    case TOP = 'top';
    case BEST = 'best';
    case NEW = 'new';
    case CONTACTS = 'contacts';
}

<?php

namespace App\Providers;

use App\Models\Category\Category;
use App\Models\Customer\Customer;
use App\Models\Order\Order;
use App\Models\Product\Product;
use App\Models\Role;
use App\Models\Setting\Setting;
use App\Models\User;
use App\Policies\CategoryPolicy;
use App\Policies\CustomerPolicy;
use App\Policies\OrderPolicy;
use App\Policies\ProductPolicy;
use App\Policies\RolePolicy;
use App\Policies\SettingPolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Role::class => RolePolicy::class,
        Category::class => CategoryPolicy::class,
        User::class => UserPolicy::class,
        Product::class => ProductPolicy::class,
        Setting::class => SettingPolicy::class,
        Order::class => OrderPolicy::class,
        Customer::class => CustomerPolicy::class,
    ];

    public function register(): void
    {
        $this->registerPolicies();
    }

    public function boot(): void
    {
    }
}

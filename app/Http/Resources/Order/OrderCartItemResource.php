<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderCartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'product_id' => $this->resource->product_id,
            'name' => $this->resource->name,
            'quantity' => $this->resource->quantity,
            'price' => $this->resource->price,
            'subtotal' => $this->resource->price * $this->resource->quantity,
            'orderId' => $this->resource->order->id,
            'orderStatus' => $this->resource->order->status,
        ];
    }
}

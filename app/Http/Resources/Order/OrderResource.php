<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\CountryResource;
use App\Http\Resources\CustomerResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'surname' => $this->resource->surname,
            'email' => $this->resource->email,
            'phone' => $this->resource->phone,
            'address' => $this->resource->address,
            'total' => $this->resource->total,
            'customer' => $this->resource->customer ? CustomerResource::collection([$this->resource->customer])[0] : null,
            'countryId' => $this->resource->country_id,
            'country' => CountryResource::collection([$this->resource->country])[0],
            'deliveryId' => $this->resource->deliveryId,
            'delivery' => $this->resource->delivery,
            'delivery_terminal_code' => $this->resource->delivery_terminal_code,
            'delivery_terminal_name' => $this->resource->delivery_terminal_name,
            'paymentId' => $this->resource->paymentId,
            'payment' => $this->resource->payment,
            'cartItems' => OrderCartItemResource::collection($this->resource->cartItems),
            'status' => $this->resource->status,
            'trackCode' => $this->resource->track_code
        ];
    }
}

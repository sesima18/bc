<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'parent_id' => ($this->resource->parent_id) ?? 0,
            'hidden' => $this->resource->hidden,
            'menu_header' => $this->resource->menu_header,
            'menu_footer_1' => $this->resource->menu_footer_1,
            'menu_footer_2' => $this->resource->menu_footer_2,
            'priority' => $this->resource->priority,
            'descriptions' => CategoryDescriptionResource::collection($this->resource->descriptions),
            'parentsList' => $this->resource->parentsList,
        ];
    }
}

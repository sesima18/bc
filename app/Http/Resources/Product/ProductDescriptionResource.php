<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductDescriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'language_id' => $this->resource->language_id,
            'name' => $this->resource->name,
            'slug' => $this->resource->slug,
            'body' => $this->resource->body
        ];
    }
}

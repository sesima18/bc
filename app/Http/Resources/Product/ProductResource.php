<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Order\OrderCartItemResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'price' => $this->resource->price,
            'sale' => $this->resource->sale,
            'quantityWeb' => $this->resource->quantityWeb,
            'quantity' => $this->resource->quantity,
            'priority' => $this->resource->priority,
            'hidden' => $this->resource->hidden,
            'top' => $this->resource->top,
            'categories' => ProductCategoryResource::collection($this->resource->categories),
            'descriptions' => ProductDescriptionResource::collection($this->resource->descriptions),
            'photos' => ProductPhotoResource::collection($this->resource->photos),
            'cartsItems' => OrderCartItemResource::collection($this->resource->orders),
        ];
    }
}

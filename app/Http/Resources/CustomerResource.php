<?php

namespace App\Http\Resources;

use App\Http\Resources\Order\OrderResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->resource->id,
            'name' => $this->resource->name,
            'surname' => $this->resource->surname,
            'email' => $this->resource->email,
            'phone' => $this->resource->phone,
            'address' => $this->resource->address,
            'country' => CountryResource::collection([$this->resource->country])[0],
            'orders' => $this->resource->orders, //to other resource not orders
            'ordersTotal' => $this->resource->orders->sum('total'),
            'no_subscription' => $this->resource->no_subscription
        ];
    }
}

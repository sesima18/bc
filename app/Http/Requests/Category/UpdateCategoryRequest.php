<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = $this->route('category');
        return [
            'parent_id' => ['nullable', 'integer'],
            'hidden' => ['nullable', 'boolean'],
            'menu_header' => ['nullable', 'boolean'],
            'menu_footer_1' => ['nullable', 'boolean'],
            'menu_footer_2' => ['nullable', 'boolean'],

            'descriptions.*.name' => ['required', 'string', 'max:255'],
            'descriptions.*.slug' => ['required', 'string', 'max:255', "unique:categories_descriptions,slug,{$id},category_id"],
            'descriptions.*.body' => ['nullable','string']
        ];
    }

    public function attributes(): array
    {
        return [
            'descriptions.*.name' => trans('validation.attributes.name'),
            'descriptions.*.slug' => trans('validation.attributes.slug'),
            'descriptions.*.body' => trans('validation.attributes.body')
        ];
    }
}

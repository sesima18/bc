<?php

namespace App\Http\Requests\Roles;

use App\Enums\Permissions;
use Illuminate\Foundation\Http\FormRequest;

class CreateRoleRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'permissions' => 'required|array',
            'permissions.*' => 'string|in:' . implode(
                    ',',
                    array_column(Permissions::cases(), 'value')
                ),
        ];
    }

    public function attributes(): array
    {
        return [
            'permissions.*' => trans('validation.attributes.permissions'),
            'permissions' => trans('validation.attributes.permissions'),
        ];
    }
}

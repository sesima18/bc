<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class StoreSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'key' => ['required'],
            'name' => ['required'],
            'tab_id' => ['required', 'exists:settings_tabs,id'],

            'descriptions.*.body' => ['required','string']
        ];
    }

    public function attributes(): array
    {
        return [
            'descriptions.*.body' => trans('validation.attributes.body')
        ];
    }
}

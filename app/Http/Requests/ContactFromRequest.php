<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactFromRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required'
        ];
        //google recaptcha
//            $secretKey = env('captcha3_secret');
//            $token = $this->input('g-recaptcha-response');
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $secretKey, 'response' => $token)));
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            $response = curl_exec($ch);
//            curl_close($ch);
//            $responseKeys = json_decode($response, true);
//            if (!$responseKeys["success"]) {
//                $rules['captcha'] = 'required';
//            }
        return $rules;
    }

    public function attributes()
    {
        return [
            'message' => trans('db.message')
        ];
    }
}

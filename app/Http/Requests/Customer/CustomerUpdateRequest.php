<?php

namespace App\Http\Requests\Customer;

use Illuminate\Foundation\Http\FormRequest;

class CustomerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation(): void
    {
        app()->setLocale(session()->get('currentLanguage')->name);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = auth()->guard('customer')->user()->id;
        return [
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['nullable', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255', "unique:customers,email,{$id},id"],
            'phone' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'countryId' => ['required', 'exists:countries,id'],
            'password' => ['nullable', 'string', 'min:8', 'confirmed'],
            'noSubscription' => ['nullable', 'boolean']
        ];
    }

    public function attributes(): array
    {
        return [
            'name' => trans('validation.attributes.user_name'),
        ];
    }
}

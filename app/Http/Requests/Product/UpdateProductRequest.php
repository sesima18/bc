<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = $this->route('product');
        return [
            'price' => ['required', 'numeric'],
            'sale' => ['nullable', 'numeric'],
            'quantity' => ['required','integer'],
            'hidden' => ['nullable', 'boolean'],
            'top' => ['nullable', 'boolean'],
            'categoriesIds' => ['array', 'required', 'min:1'],
            "photos" => ["array", "max:5"],
            'photos.*' => ['nullable', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            "descriptions" => ['required', "array", "min:1"],
            'descriptions.*.name' => ['required', 'string', 'max:255'],
            'descriptions.*.slug' => ['required', 'string', 'max:255', "unique:products_descriptions,slug,{$id},product_id"],
            'descriptions.*.body' => ['nullable','string']
        ];
    }

    public function attributes(): array
    {
        return [
            'descriptions.*.name' => trans('validation.attributes.name'),
            'descriptions.*.slug' => trans('validation.attributes.slug'),
            'descriptions.*.body' => trans('validation.attributes.body'),
            'categoriesIds' => trans('validation.attributes.categoriesIds'),
            'price' => trans('validation.attributes.price'),
            'quantity' => trans('validation.attributes.quantity'),
            'photos.*' => trans('validation.attributes.photos'),
        ];
    }
}

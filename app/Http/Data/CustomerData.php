<?php

namespace App\Http\Data;

use App\Http\Requests\Customer\CustomerRequest;
use App\Models\Customer\Customer;

class CustomerData
{
    public ?int $id = null;
    public ?string $name = null;
    public ?string $surname = null;
    public ?string $email = null;
    public ?string $phone = null;
    public ?string $address = null;
    public ?int $countryId = 125;
    public ?bool $noSubscription = false;

    public function create(?Customer $customer): CustomerData {
        $this->id = $customer->id;
        $this->name = $customer->name;
        $this->surname = $customer->surname;
        $this->email = $customer->email;
        $this->phone = $customer->phone;
        $this->address = $customer->address;
        $this->countryId = $customer->country_id;
        $this->noSubscription = $customer->no_subscription;
        return $this;
    }
}

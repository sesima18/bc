<?php

namespace App\Http\Data;

class WebData
{
    public string $template;
    public array $data;
}

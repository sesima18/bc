<?php

namespace App\Http\Data;

use App\Http\Requests\Checkout\CheckoutRequest;
use App\Models\Order\Order;
use App\ValueObjects\Money;

class CheckoutData
{
    public ?int $customerId = null;
    public ?string $name = null;
    public ?string $surname = null;
    public ?string $email = null;
    public ?string $phone = null;
    public ?string $address = null;
    public ?int $countryId = 125;
    public ?string $country= null;
    public ?string $comment = null;
    public ?int $deliveryId = null;
    public string $delivery;
    public ?string $deliveryTerminalCode = null;
    public ?string $deliveryTerminalName = null;
    public int $paymentId;
    public string $payment;
    /**
     * @var CartData[]
     */
    public array $cartItems;
    public ?Money $total = null;

    public function create(
        CheckoutRequest $request,
        CountryData $country,
        DeliveryData $delivery,
        PaymentData $payment,
        array $cartItems,
        Money $total,
        ?int $customerId = null,
        ?DeliveryTerminalData $deliveryTerminal = null
    ): CheckoutData {
        $this->customerId = $customerId;
        $this->name = $request->input('name');
        $this->surname = $request->input('surname');
        $this->email = $request->input('email');
        $this->phone = $request->input('phone');
        $this->address = $request->input('address');
        $this->countryId = $request->input('countryId');

        $this->deliveryId = $request->input('deliveryId');
        $this->paymentId = $request->input('paymentId');
        $this->comment = $request->input('comment');

        $this->country = $country->name;
        $this->delivery = $delivery->name;
        if($deliveryTerminal) {
            $this->deliveryTerminalCode = $deliveryTerminal->id;
            $this->deliveryTerminalName = $deliveryTerminal->name;
        }
        $this->payment = $payment->name;
        $this->cartItems = $cartItems;
        $this->total = $total;
        return $this;
    }

    public function createFromCustomerData(
        CustomerData $customer
    ): CheckoutData {
        $this->customerId = $customer->id;
        $this->name = $customer->name;
        $this->surname = $customer->surname;
        $this->email = $customer->email;
        $this->phone = $customer->phone;
        $this->address = $customer->address;
        $this->countryId = $customer->countryId;
        return $this;
    }
}

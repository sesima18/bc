<?php

namespace App\Http\Data;

use App\ValueObjects\Money;
use Gloudemans\Shoppingcart\CartItem;

class CartData
{
    public int $id;
    public string $rowId;
    public string $name;
    public Money $priceMoney;
    public ?Money $saleMoney;
    public ?string $link;
    public string $photo;
    public int $quantity;
    public Money $subtotal;

    public function create(CartItem $cartItem, ProductData $productData): self
    {
        $this->id = $productData->id;
        $this->rowId = $cartItem->rowId;
        $this->priceMoney = $productData->priceMoney;
        $this->saleMoney = $productData->saleMoney;
        $this->name = $productData->name;
        $this->link = $productData->link;
        $this->photo = $productData->photo;
        $this->quantity = $cartItem->qty;
        $this->subtotal = new Money($cartItem->subtotal);
        return $this;
    }
}

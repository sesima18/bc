<?php

namespace App\Http\Data;

use App\Models\Delivery\DeliveryTerminal;

class DeliveryTerminalData
{
    public string $id;
    public string $name;
    public string $city;

    public function create(DeliveryTerminal $deliveryTerminal): self
    {
        $this->id = $deliveryTerminal->code;
        $this->name = $deliveryTerminal->name;
        $this->city = $deliveryTerminal->city;
        return $this;
    }
}

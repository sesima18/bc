<?php

namespace App\Http\Data;

use App\Models\Order\OrderCartItem;
use App\ValueObjects\Money;

class OrderCartData
{
    public string $name;
    public Money $price;
    public int $quantity;
    public string $link;
    public Money $subtotal;

    public function create(OrderCartItem $cartItem, ProductData $productData): self
    {
        $this->name = $cartItem->name;
        $this->price = new Money($cartItem->price);
        $this->quantity = $cartItem->quantity;
        $this->link = $productData->link;
        $this->subtotal = new Money($cartItem->price * $cartItem->quantity);
        return $this;
    }
}

<?php

namespace App\Http\Data;

use App\Models\Product\Product;
use App\Models\Product\ProductDescription;
use App\ValueObjects\Money;
use Carbon\Carbon;

class ProductData
{
    public int $id;
    public string $name;
    public Money $priceMoney;
    public ?Money $saleMoney;

    public ?string $link;
    public string $photo;
    public $photos;
    public ?string $body;
    public bool $isTop = false;
    public bool $isNew = false;
    public bool $isBest = false;
    public int $quantity = 0;
    public int $categoryId;

    public function create(Product $product, ProductDescription $productByLocation, ?string $link = null): self
    {
        $this->id = $product->id;
        $this->priceMoney = ($product->sale) ? new Money($product->sale) :  new Money($product->price);
        $this->saleMoney = ($product->sale) ? new Money($product->price) : null;
        $this->isTop = $product->top;
        $this->isBest = (bool)($product->sale);
        $this->isNew = ($product->created_at  > Carbon::today()->subDays(env('PRODUCT_NEW_DAYS')));
        $this->photo = ($product->photos()->first()->photoUrl) ?? '/images/no-image.jpg';
        $this->photos =  ($product->photos()->count() > 0) ? $product->photos->map->only('photoUrl') : [['photoUrl' => '/images/no-image.jpg']];
        $this->name = $productByLocation->name;
        $this->body = $productByLocation->body;
        $this->link = $link;
        $this->quantity = $product->quantityWeb;
        $this->categoryId = $product->categories()->first()->category_id;
        return $this;
    }
}

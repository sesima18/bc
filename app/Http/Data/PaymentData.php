<?php

namespace App\Http\Data;

use App\Models\Payment\Payment;
use App\Models\Payment\PaymentDescription;

class PaymentData
{
    public int $id;
    public string $name;

    public function create(Payment $payment, PaymentDescription $paymentDescription): self
    {
        $this->id = $payment->id;
        $this->name = $paymentDescription->name;
        return $this;
    }
}

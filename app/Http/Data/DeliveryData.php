<?php

namespace App\Http\Data;


use App\Models\Delivery\Delivery;
use App\Models\Delivery\DeliveryDescription;

class DeliveryData
{
    public int $id;
    public string $country_id;
    public string $name;
    public array $terminals;

    public function create(
        Delivery $delivery,
        DeliveryDescription $deliveryDescription,
        $terminals = [],
        $cities = []
    ): self {
        $this->id = $delivery->id;
        $this->name = $deliveryDescription->name;
        $this->country_id = $delivery->country_id;

        $cities = $delivery->cities;
        $cities = $this->cityTop($cities, 'Panevėžio m.');
        $cities = $this->cityTop($cities, 'Šiaulių m.');
        $cities = $this->cityTop($cities, 'Klaipėdos m.');
        $cities = $this->cityTop($cities, 'Kauno m.');
        $cities = $this->cityTop($cities, 'Vilniaus m.');

        $cities = $this->cityTop($cities, 'PANEVĖŽYS');
        $cities = $this->cityTop($cities, 'ŠIAULIAI');
        $cities = $this->cityTop($cities, 'KLAIPĖDA');
        $cities = $this->cityTop($cities, 'KAUNAS');
        $cities = $this->cityTop($cities, 'VILNIUS');

        $terminals2 = [];
        foreach ($cities as $city) {
            $terminals2[$city] = array_filter($terminals, fn($terminal) => $terminal->city == $city);
        }
        $this->terminals = $terminals2;
        return $this;
    }

    private function cityTop($cities, $city)
    {
        $key = array_search($city, $cities);
        if($key) {
            unset($cities[$key]);
            array_unshift($cities, $city);
        }
        return $cities;
    }
}

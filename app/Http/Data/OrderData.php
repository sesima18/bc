<?php

namespace App\Http\Data;

use App\Enums\OrderStatuses;
use App\Models\Order\Order;
use App\ValueObjects\Money;

class OrderData
{
    public int $id;
    public string $status;
    public string $statusClass;
    public Money $total;
    public array $products;
    public ?string $trackCode;
    public string $delivery;
    public ?string $deliveryTerminal;

    public ?string $name = null;
    public ?string $surname = null;
    public ?string $email = null;
    public ?string $phone = null;
    public ?string $address = null;
    public ?string $country= null;
    public ?string $comment = null;
    public string $payment;

    /**
     * @param OrderCartData[] $products
     */
    public function create(Order $order, array $products): self
    {
        $orderStatuses = [
            '',
            OrderStatuses::STATUS_NEW => trans('db.orderStatusNew'),
            OrderStatuses::STATUS_PAID => trans('db.orderStatusPaid'),
            OrderStatuses::STATUS_SEND => trans('db.orderStatusSend'),
            OrderStatuses::STATUS_CANCELLED => trans('db.orderStatusCancelled'),
            OrderStatuses::STATUS_ENDED => trans('db.orderStatusEnded')
        ];
        $orderClasses = [
            '',
            'btn-outline-primary',
            'btn-outline-danger',
            'btn-outline-warning',
            'btn-outline-secondary',
            'btn-outline-success'
        ];
        $this->id = $order->id;
        $this->status = $orderStatuses[$order->status];
        $this->statusClass = $orderClasses[$order->status];
        $this->total = new Money($order->total);
        $this->products = $products;
        $this->trackCode = $order->track_code;
        $this->delivery = $order->delivery;
        $this->deliveryTerminal = $order->delivery_terminal_name;

        $this->name = $order->name;
        $this->surname = $order->surname;
        $this->email = $order->email;
        $this->phone = $order->phone;
        $this->address = $order->address;
        $this->country = $order->country->code;
        $this->payment = $order->payment;
        $this->comment = $order->comment;
        return $this;
    }
}

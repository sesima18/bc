<?php

namespace App\Http\Data;

use App\Enums\CategoryTypes;
use App\Models\Category\Category;
use App\Models\Category\CategoryDescription;

class CategoryData
{
    public int $id;
    public string $name;
    public ?string $body;
    public ?string $link = null;
    public bool $isInPath = false;
    public ?CategoryTypes $categoryType = null;

    /** @var CategoryData[]  */
    public ?array $submenu = null;
    public int $productsTotal = 0;

    public function create(
        Category $category,
        CategoryDescription $categoryByLocation,
        ?CategoryTypes $categoryType = null,
        ?string $link = null,
        bool $isInPath = false,
        ?array $submenu = [],
        int $productsTotal = 0
    ): self {
        $this->id = $category->id;
        $this->name = $categoryByLocation->name;
        $this->body = $categoryByLocation->body;
        $this->categoryType = $categoryType;
        $this->link = $link;
        $this->isInPath = $isInPath;
        $this->submenu = $submenu;
        $this->productsTotal = $category->products()->count();
        return $this;
    }
}

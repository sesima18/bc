<?php

namespace App\Http\Data;

use App\Models\Country\Country;
use App\Models\Country\CountryDescription;

class CountryData
{
    public int $id;
    public string $name;

    public function create(Country $country, CountryDescription $countryDescription): self
    {
        $this->id = $country->id;
        $this->name = $countryDescription->name;
        return $this;
    }
}

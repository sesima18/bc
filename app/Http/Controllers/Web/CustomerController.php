<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Api\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Customer\CustomerRequest;
use App\Http\Requests\Customer\CustomerUpdateRequest;
use App\Http\Requests\Customer\ForgotPasswordRequest;
use App\Http\Requests\Customer\ResetPasswordRequest;
use App\Models\Customer\Customer;
use App\Notifications\Customer as CustomerNotification;
use App\Services\LanguageService;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use \Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class CustomerController extends Controller
{
    public function login(LoginRequest $request): RedirectResponse
    {
        if (Auth::guard('customer')->attempt($request->validated())) {
            return redirect()->back();
        }
        return redirect()->back()->with(['loginError' => 'OK']);
    }

    public function logout(): RedirectResponse
    {
        Auth::guard('customer')->logout();
        return redirect()->back();
    }

    public function register(CustomerRequest $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $validatedData['country_id'] = $validatedData['countryId'];
        $validatedData['no_subscription'] = ($validatedData['noSubscription']) ?? 0;
        $customer = Customer::create($validatedData);
        Auth::guard('customer')->login($customer);
        $customer->notify(
            new CustomerNotification(
                $customer,
                trans('db.letterRegisterSubject'),
                trans('db.letterRegister.html'),
                'emails.register'
            )
        );
        return redirect()->back();
    }

    public function profile(CustomerUpdateRequest $request): RedirectResponse
    {
        $validatedData = $request->validated();
        $validatedData['country_id'] = $validatedData['countryId'];
        $validatedData['no_subscription'] = ($validatedData['noSubscription']) ?? 0;
        Auth::guard('customer')->user()->update($validatedData);
        return redirect()->back()->with(['profileSuccess' => 'OK']);
    }

    public function forgotPassword(ForgotPasswordRequest $request): RedirectResponse
    {
        $status = Password::broker('customers')->sendResetLink(
            $request->only('email')
        );

        return $status === Password::RESET_LINK_SENT
            ? back()->with(['forgotPasswordSuccess' => 'OK'])
            : back()->withErrors(['email' => __($status)]);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $languageService = new LanguageService();
        $languageService->getCurrent($request);
        $status = Password::broker('customers')->reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function (Customer $user, string $password) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset($user));
            }
        );

        return $status === Password::PASSWORD_RESET
            ? redirect()->route('login', $languageService->getCurrent())->with('restPasswordSuccess', 'OK')
            : back()->withErrors(['email' => [__($status)]]);
    }
}

<?php

namespace App\Http\Controllers\Web;

use App\Enums\OrderStatuses;
use App\Http\Controllers\Api\Controller;
use App\Http\Data\CheckoutData;
use App\Http\Data\OrderCartData;
use App\Http\Data\OrderData;
use App\Http\Requests\Checkout\CheckoutRequest;
use App\Models\Order\Order;
use App\Notifications\Purchase;
use App\Services\CartService;
use App\Services\CategoryService;
use App\Services\CountryService;
use App\Services\DeliveryService;
use App\Services\LanguageService;
use App\Services\PaymentService;
use App\Services\ProductService;
use App\ValueObjects\Money;
use \Cart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function add(Request $request, int $productId, int $qty): array
    {
        $error = '';
        $cartItem = Cart::search(function ($cartItem) use ($productId) {
            return $cartItem->id === $productId;
        })->first();
        if (empty($cartItem)) {
            $cartService = new CartService();
            $cartItem = $cartService->addCartItem($productId);
        }
        $languageService = new LanguageService();
        $currentLanguage = $languageService->getCurrent($request);
        $productService = new ProductService($currentLanguage);
        $productData = $productService->getProductDataById($productId);
        if ($productData->quantity < $qty) {
            $error = sprintf('Prekės %s didžiausias galimas kiekis: %s', $productData->name, $productData->quantity);
        }
        $quantity = min($qty, $productData->quantity);
        Cart::update($cartItem->rowId, $quantity);
        return ['total' => (string)new Money(Cart::total()), 'quantity' => $quantity, 'error' => $error];
    }

    public function checkout(CheckoutRequest $request): RedirectResponse
    {
        $languageService = new LanguageService();
        $languageService->getCurrent($request);

        $countryService = new CountryService();
        $country = $countryService->getCountryDataById($request->input('countryId'));

        $deliveryService = new DeliveryService();
        $delivery = $deliveryService->getDeliveryDataById($request->input('deliveryId'));
        $deliveryTerminal = null;
        if($delivery) {
            $deliveryTerminalInput = $request->input("deliveryTerminal");
            if(isset($deliveryTerminalInput[$delivery->id])) {
                $deliveryTerminal = $deliveryService->getDeliveryTerminalDataByCode(
                    $deliveryTerminalInput[$delivery->id]
                );
            }
        }

        $paymentService = new PaymentService();
        $payment = $paymentService->getPaymentDataById($request->input('paymentId'));

        $cartService = new CartService();
        $cartItems = $cartService->getCartItems();

        $total = new Money(Cart::total());
        $customerId = (\Auth::guard('customer')->check()) ? \Auth::guard('customer')->user()->id : null;
        $checkoutData = (new CheckoutData())->create(
            $request,
            $country,
            $delivery,
            $payment,
            $cartItems,
            $total,
            $customerId,
            $deliveryTerminal
        );
        $request->session()->put('checkoutData', $checkoutData);

        return redirect(url()->previous() . '/preview');
    }

    public function pay(Request $request)
    {
        $languageService = new LanguageService();
        $currentLanguage = $languageService->getCurrent($request);
        $categoryService = new CategoryService();
        $orderCategory = $categoryService->getCategoryDataById(10);
        $productService = new ProductService();
        $checkoutData = $request->session()->get('checkoutData');
        $orderData = (array)$checkoutData;
        $orderData['country_id'] = $checkoutData->countryId;
        $orderData['delivery_id'] = $checkoutData->deliveryId;
        $orderData['payment_id'] = $checkoutData->paymentId;
        $orderData['customer_id'] = $checkoutData->customerId;
        $orderData['delivery_terminal_code'] = $checkoutData->deliveryTerminalCode;
        $orderData['delivery_terminal_name'] = $checkoutData->deliveryTerminalName;
        $orderData['status'] = OrderStatuses::STATUS_NEW;
        $orderData['total'] = $checkoutData->total->price;
        $order = Order::create($orderData);
        $total = 0;
        foreach ($checkoutData->cartItems as $cartItem) {
            $product = $productService->getProductDataById($cartItem->id);
            $cartItem->quantity = min($cartItem->quantity, $product->quantity);
            $cartItem->product_id = $cartItem->id;
            $cartItem->price = $cartItem->priceMoney->price;
            $order->cartItems()->create((array)$cartItem);
            $total += $cartItem->price * $cartItem->quantity;
        }
        $order->update(['total' => $total, 'language_id' => session()->get('currentLanguage')->id]);

        $productsService = new ProductService();
        $orderDataProducts = $order->cartItems->map(function($orderCartItem) use ($productsService) {
            $productData = $productsService->getProductDataById($orderCartItem->product_id);
            return (new OrderCartData())->create($orderCartItem, $productData);
        })->toArray();
        $orderData = (new OrderData())->create($order, $orderDataProducts);
        $order->notify(new Purchase($orderData, trans('db.letterOrderSubject'), null, 'emails.purchase'));

        $request->session()->forget('checkoutData');
        \Cart::destroy();

        return redirect($orderCategory->link . '/paid');
    }
}

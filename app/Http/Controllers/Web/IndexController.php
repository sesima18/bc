<?php

namespace App\Http\Controllers\Web;

use App\Enums\CategoryTypes;
use App\Http\Controllers\Api\Controller;
use App\Http\Requests\ContactFromRequest;
use App\Mail\LetterSimple;
use App\Models\Country\Country;
use App\Models\Delivery\DeliveryTerminal;
use App\Services\CategoryService;
use App\Services\LanguageService;
use App\Services\WebService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class IndexController extends Controller
{
    public function index(Request $request): View
    {
        $languageService = new LanguageService();
        $currentLanguage = $languageService->getCurrent($request);

        $categoryService = new CategoryService();
        $currentCategory = $categoryService->getCurrent($request);
        if (empty($currentCategory)) {
            abort(404);
        }

        $webService = new WebService($currentCategory, $categoryService, $currentLanguage);

        $webData = $webService->getIndex();

        if ($currentCategory->categoryType == CategoryTypes::HOME) {
            $webData = $webService->getHome();
        }

        if ($currentCategory->categoryType == CategoryTypes::PRODUCTS) {
            $webData = $webService->getProducts($request);
        }

        if ($currentCategory->categoryType == CategoryTypes::TOP) {
            $webData = $webService->getProducts($request, true);
        }

        if ($currentCategory->categoryType == CategoryTypes::NEW) {
            $webData = $webService->getProducts($request, false, true);
        }

        if ($currentCategory->categoryType == CategoryTypes::BEST) {
            $webData = $webService->getProducts($request, false, false, true);
        }

        if ($currentCategory->categoryType == CategoryTypes::SEARCH) {
            $webData = $webService->getSearch($request);
        }

        if ($currentCategory->categoryType == CategoryTypes::CART) {
            $webData = $webService->getCart();
        }

        if ($currentCategory->categoryType == CategoryTypes::CHECKOUT) {
            $webData = $webService->getCheckout($request);
        }

        if ($currentCategory->categoryType == CategoryTypes::CUSTOMER) {
            $webData = $webService->getCustomer($request);
        }

        $pageData = [];
        foreach ($webData->data as $variableName => $variableValue) {
            $$variableName = $variableValue;
            $pageData[] = $variableName;
        }
        return view($webData->template, compact($pageData));
    }

    public function resetPassword(Request $request): View
    {
        $languageService = new LanguageService();
        $currentLanguage = $languageService->getCurrent($request);

        $categoryService = new CategoryService();
        $currentCategory = $categoryService->getCategoryDataById(11);

        $webService = new WebService($currentCategory, $categoryService, $currentLanguage);

        $webData = $webService->getCustomer($request);
        $pageData = [];
        foreach ($webData->data as $variableName => $variableValue) {
            $$variableName = $variableValue;
            $pageData[] = $variableName;
        }
        return view($webData->template, compact($pageData));
    }

    public function contacts(ContactFromRequest $request)
    {
        \Mail::to(trans('db.pageEmail'))->send(new LetterSimple('Kontaktų forma', 'emails.contact', [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'body' => $request->input('message'),
        ]));
        return response()->json();
    }

    private function addTerminalsFromDB()
    {
        $terminals = \DB::table('terminals')->get();
        foreach ($terminals as $terminal) {
            $carrier = ($terminal->carrier == 'DPD') ? 3 : 2;
            DeliveryTerminal::create(
                ['delivery_id' => $carrier, 'code' => $terminal->code, 'name' => $terminal->name, 'address' => $terminal->address,
                    'city' => $terminal->city, 'postal_code' => $terminal->post_code, 'country' => $terminal->country]
            );

        }
    }

    private function addTerminalsLP()
    {
        $xml = simplexml_load_file(
            'https://api.balticpost.lt/download/terminallist/?key=CM35D7WMYDCA',
            'SimpleXMLElement',
            LIBXML_NOCDATA
        );
        $json = json_encode($xml);
        $data = json_decode($json, true);
        foreach ($data['data']['item'] as $one) {
            if ($one['address']['country'] == 'LT') {
                DeliveryTerminal::create(
                    ['delivery_id' => 2, 'code' => $one['id'], 'name' => $one['name'], 'address' => $one['address']['streethouse'],
                        'city' => $one['address']['city'], 'postal_code' => $one['address']['postcode'], 'country' => 'LT']
                );
            }
        }
    }
    //todo:
    //pastomatai pagal salis
    //capcha

    //mobile !!!
    //netrinti produku, jei order

private function addCountries()
{
    $countries = '1;AF;افغانستان
2;AX;Åland
3;AL;Shqipëria
4;DZ;الجزائر
5;AS;American Samoa
6;AD;Andorra
7;AO;Angola
8;AI;Anguilla
9;AQ;Antarctica
10;AG;Antigua and Barbuda
11;AR;Argentina
12;AM;Հայաստան
13;AW;Aruba
14;AU;Australia
15;AT;Österreich
16;AZ;Azərbaycan
18;BH;‏البحرين
19;BD;Bangladesh
20;BB;Barbados
21;BY;Белару́сь
22;BE;België
23;BZ;Belize
24;BJ;Bénin
25;BM;Bermuda
26;BT;ʼbrug-yul
27;BO;Bolivia
155;BQ;Caribisch Nederland
28;BA;Bosna i Hercegovina
29;BW;Botswana
30;BV;Bouvetøya
31;BR;Brasil
32;IO;British Indian Ocean Territory
33;BN;Negara Brunei Darussalam
34;BG;България
35;BF;Burkina Faso
36;BI;Burundi
37;KH;Kâmpŭchéa
38;CM;Cameroon
39;CA;Canada
40;CV;Cabo Verde
41;KY;Cayman Islands
42;CF;Ködörösêse tî Bêafrîka
43;TD;Tchad
44;CL;Chile
45;CN;中国
46;CX;Christmas Island
47;CC;Cocos (Keeling) Islands
48;CO;Colombia
49;KM;Komori
50;CG;République du Congo
52;CK;Cook Islands
53;CR;Costa Rica
54;CI;
55;HR;Hrvatska
56;CU;Cuba
249;CW;Curaçao
57;CY;Κύπρος
58;CZ;Česká republika
51;CD;République démocratique du Congo
59;DK;Danmark
60;DJ;Djibouti
61;DM;Dominica
62;DO;República Dominicana
64;EC;Ecuador
65;EG;مصر‎
66;SV;El Salvador
67;GQ;Guinea Ecuatorial
68;ER;ኤርትራ
69;EE;Eesti
212;SZ;Swaziland
70;ET;ኢትዮጵያ
71;FK;Falkland Islands
72;FO;Føroyar
73;FJ;Fiji
74;FI;Suomi
75;FR;France
76;GF;Guyane française
77;PF;Polynésie française
78;TF;Territoire des Terres australes et antarctiques fr
79;GA;Gabon
81;GE;საქართველო
82;DE;Deutschland
83;GH;Ghana
84;GI;Gibraltar
85;GR;Ελλάδα
86;GL;Kalaallit Nunaat
87;GD;Grenada
88;GP;Guadeloupe
89;GU;Guam
90;GT;Guatemala
91;GG;Guernsey
92;GN;Guinée
93;GW;Guiné-Bissau
94;GY;Guyana
95;HT;Haïti
96;HM;Heard Island and McDonald Islands
97;HN;Honduras
98;HK;香港
99;HU;Magyarország
100;IS;Ísland
101;IN;भारत
102;ID;Indonesia
103;IR;ایران
104;IQ;العراق
105;IE;Éire
106;IL;יִשְׂרָאֵל
107;IT;Italia
108;JM;Jamaica
109;JP;日本
110;JE;Jersey
111;JO;الأردن
112;KZ;Қазақстан
113;KE;Kenya
114;KI;Kiribati
248;XK;Republika e Kosovës
117;KW;الكويت
118;KG;Кыргызстан
119;LA;ສປປລາວ
120;LV;Latvija
121;LB;لبنان
122;LS;Lesotho
123;LR;Liberia
124;LY;‏ليبيا
125;LI;Liechtenstein
126;LT;Lietuva
127;LU;Luxembourg
128;MO;澳門
130;MG;Madagasikara
131;MW;Malawi
132;MY;Malaysia
133;MV;Maldives
134;ML;Mali
135;MT;Malta
136;IM;Isle of Man
137;MH;M̧ajeļ
138;MQ;Martinique
139;MR;موريتانيا
140;MU;Maurice
141;YT;Mayotte
142;MX;México
143;FM;Micronesia
144;MD;Moldova
145;MC;Monaco
146;MN;Монгол улс
147;ME;Црна Гора
148;MS;Montserrat
149;MA;المغرب
150;MZ;Moçambique
151;MM;မြန်မာ
152;NA;Namibia
153;NR;Nauru
154;NP;नपल
156;NL;Nederland
157;NC;Nouvelle-Calédonie
158;NZ;New Zealand
159;NI;Nicaragua
160;NE;Niger
161;NG;Nigeria
162;NU;Niuē
163;NF;Norfolk Island
115;KP;북한
129;MK;Северна Македонија
164;MP;Northern Mariana Islands
165;NO;Norge
166;OM;عمان
167;PK;Pakistan
168;PW;Palau
169;PS;فلسطين
170;PA;Panamá
171;PG;Papua Niugini
172;PY;Paraguay
173;PE;Perú
174;PH;Pilipinas
175;PN;Pitcairn Islands
176;PL;Polska
177;PT;Portugal
178;PR;Puerto Rico
179;QA;قطر
180;RE;La Réunion
181;RO;România
182;RU;Россия
183;RW;Rwanda
184;SH;Saint Helena
185;KN;Saint Kitts and Nevis
186;LC;Saint Lucia
187;PM;Saint-Pierre-et-Miquelon
188;VC;Saint Vincent and the Grenadines
189;BL;Saint-Barthélemy
190;MF;Saint-Martin
191;WS;Samoa
192;SM;San Marino
193;ST;São Tomé e Príncipe
194;SA;المملكة العربية السعودية
195;SN;Sénégal
196;RS;Србија
197;SC;Seychelles
198;SL;Sierra Leone
199;SG;Singapore
250;SX;Sint Maarten
200;SK;Slovensko
201;SI;Slovenija
202;SB;Solomon Islands
203;SO;Soomaaliya
204;ZA;South Africa
205;GS;South Georgia
116;KR;대한민국
206;SS;South Sudan
207;ES;España
208;LK;śrī laṃkāva
209;SD;السودان
210;SR;Suriname
211;SJ;Svalbard og Jan Mayen
213;SE;Sverige
214;CH;Schweiz
215;SY;سوريا
216;TW;臺灣
217;TJ;Тоҷикистон
218;TZ;Tanzania
219;TH;ประเทศไทย
17;BS;Bahamas
80;GM;Gambia
63;TL;Timor-Leste
220;TG;Togo
221;TK;Tokelau
222;TO;Tonga
223;TT;Trinidad and Tobago
224;TN;تونس
225;TR;Türkiye
226;TM;Türkmenistan
227;TC;Turks and Caicos Islands
228;TV;Tuvalu
229;UG;Uganda
230;UA;Україна
231;AE;دولة الإمارات العربية المتحدة
232;GB;United Kingdom
233;US;United States
234;UM;United States Minor Outlying Islands
235;UY;Uruguay
236;UZ;O‘zbekiston
237;VU;Vanuatu
238;VA;Vaticano
239;VE;Venezuela
240;VN;Việt Nam
241;VG;British Virgin Islands
242;VI;United States Virgin Islands
243;WF;Wallis et Futuna
244;EH;الصحراء الغربية
245;YE;اليَمَن
246;ZM;Zambia
247;ZW;Zimbabwe
';
    $countries = explode(PHP_EOL, $countries);
    foreach ($countries as $cc) {
        $line = explode(';', $cc);
        if(isset($line[1])) {
            $county = Country::create(['code' => $line[1]]);
            $county->descriptions()->firstOrCreate(['language_id' => 1, 'name' => $line[2]]);
            $county->descriptions()->firstOrCreate(['language_id' => 2, 'name' => $line[2]]);
        }
    }
}
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Setting\StoreSettingRequest;
use App\Http\Requests\Setting\UpdateSettingRequest;
use App\Http\Resources\Setting\SettingResource;
use App\Http\Resources\Setting\SettingTabResource;
use App\Models\Setting\Setting;
use App\Models\Setting\SettingTab;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpFoundation\Response;

class SettingController extends Controller
{
    public function index(Request $request): AnonymousResourceCollection
    {
        $this->authorize('index', Setting::class);

        $settings = Setting::orderBy('name');
        if ($request->has('query')) {
            $settings->whereHas('descriptions', function ($query) use ($request) {
                $query->where('key', 'LIKE', '%' . $request->get('query') . '%');
                $query->orWhere('name', 'LIKE', '%' . $request->get('query') . '%');
            });
        }

        return SettingResource::collection($settings->get());
    }

    public function tabs(): AnonymousResourceCollection
    {
        $this->authorize('index', Setting::class);

        $settingsTabs = SettingTab::all();

        return SettingTabResource::collection($settingsTabs);
    }

    public function store(StoreSettingRequest $request): JsonResponse
    {
        $this->authorize('store', Setting::class);

        $validatedData = $request->validated();
        $setting = Setting::create($validatedData);
        $this->saveDescriptions($setting, $validatedData);
        return new JsonResponse(Response::HTTP_CREATED);
    }

    public function update(UpdateSettingRequest $request, string $id): JsonResponse
    {
        $this->authorize('update', Setting::class);

        $validatedData = $request->validated();
        $setting = Setting::findOrFail($id);
        $setting->update($validatedData);
        $this->saveDescriptions($setting, $validatedData);
        $setting->refresh();
        return new JsonResponse(Response::HTTP_OK);
    }

    public function show(Setting $setting): SettingResource
    {
        $this->authorize('show', Setting::class);

        return SettingResource::make($setting);
    }

    public function findByKey(string $key): JsonResponse
    {
        $this->authorize('show', Setting::class);

        $webCategories = explode(',', env('WEB_CATEGORIES'));
        $webCategoriesMap = array_combine(
            array_map(fn($webCategory) => explode(':', $webCategory)[1], $webCategories),
            array_map(fn($webCategory) => explode(':', $webCategory)[0], $webCategories)
        );
        if($key == 'unDeletableCategories') {
            return response()->json(array_values($webCategoriesMap));
        }
        return response()->json($webCategoriesMap[$key]);
    }

    public function destroy(Setting $setting): JsonResponse
    {
        $this->authorize('delete', Setting::class);

        $setting->descriptions()->delete();
        $setting->delete();
        return new JsonResponse(Response::HTTP_OK);
    }

    private function saveDescriptions(Setting $setting, array $validatedData): void
    {
        $descriptions = [];
        if (isset($validatedData['descriptions'])) {
            foreach ($validatedData['descriptions'] as $languageId => $validatedDescription) {
                $settingDescription = $setting->descriptions()->firstOrCreate(['language_id' => $languageId]);
                $settingDescription->update($validatedDescription);
                $descriptions[] = $settingDescription;
            }
        }
        $setting->descriptions()->saveMany($descriptions);
    }
}

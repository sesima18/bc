<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\LanguageResource;
use App\Models\Language;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class LanguageController extends Controller
{
    public function index(): JsonResponse
    {
        $languages = LanguageResource::collection(Language::get()->keyBy->id);
        return new JsonResponse([
            'languages' => $languages
        ], Response::HTTP_OK);
    }

}

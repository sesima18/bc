<?php

namespace App\Http\Controllers\Api;

use App\Enums\Permissions;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(LoginRequest $request): JsonResponse
    {
        if (!Auth::attempt($request->validated())) {
            return new JsonResponse(
                ['message' => trans('User not found!')],
                Response::HTTP_UNAUTHORIZED,
            );
        }
        $user = Auth::user();

        return new JsonResponse(
            [
                'token' => $user->createToken('login')
                    ->plainTextToken,
                'user' => new UserResource($user),
                'permissions' => [
                    Permissions::LIST_USERS,
                    Permissions::LIST_ROLES,
                ]
            ],
            Response::HTTP_OK
        );
    }

    public function permissions(): AnonymousResourceCollection
    {
        $user = Auth::user();

        return PermissionResource::collection($user->role->permissions);
    }

    public function logout(): JsonResponse
    {
        Auth::logout();
        return new JsonResponse(Response::HTTP_OK);
    }
}

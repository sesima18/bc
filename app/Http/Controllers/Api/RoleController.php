<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Roles\CreateRoleRequest;
use App\Http\Resources\RoleResource;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class RoleController extends Controller
{
    public function index(): JsonResponse
    {
        $this->authorize('index', Role::class);

        $roles = Role::with('permissions')->orderBy('id', 'DESC')->get();
        return new JsonResponse([
            'roles' => RoleResource::collection($roles)
        ], Response::HTTP_OK);
    }

    public function store(CreateRoleRequest $request): JsonResponse
    {
        $this->authorize('store', Role::class);

        $validatedData = $request->validated();
        $role = Role::create($validatedData);
        $this->savePermissions($role, $validatedData['permissions']);

        return new JsonResponse(Response::HTTP_CREATED);
    }

    public function update(CreateRoleRequest $request, string $id): JsonResponse
    {
        $this->authorize('update', Role::class);

        $validatedData = $request->validated();
        $role = Role::findOrFail($id);
        $role->update($validatedData);
        $role->permissions()->delete();
        $this->savePermissions($role, $validatedData['permissions']);

        return new JsonResponse(Response::HTTP_OK);
    }

    public function show(Role $role): RoleResource
    {
        $this->authorize('show', Role::class);

        return RoleResource::make($role);
    }

    public function destroy(string $id): JsonResponse
    {
        $this->authorize('delete', Role::class);

        $role = Role::findOrFail($id);
        $role->permissions()->delete();
        $role->delete();

        return new JsonResponse(Response::HTTP_OK);
    }

    private function savePermissions(Role $role, array $permissions): void
    {
        foreach ($permissions as $permission) {
            Permission::create(
                [
                    'key' => $permission,
                    'role_id' => $role->id
                ]
            );
        }
    }
}

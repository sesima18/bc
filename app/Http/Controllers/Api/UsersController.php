<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    public function index(): JsonResponse
    {
        $this->authorize('index', User::class);

        $users = User::orderBy('id', 'DESC')->get();
        return new JsonResponse([
            'users' => UserResource::collection($users)
        ], Response::HTTP_OK);
    }

    public function store(StoreUserRequest $request): JsonResponse
    {
        $this->authorize('store', User::class);

        $validatedData = $request->validated();
        User::create($validatedData);
        return new JsonResponse(Response::HTTP_CREATED);
    }

    public function update(UpdateUserRequest $request, string $id): JsonResponse
    {
        $this->authorize('update', User::class);

        $validatedData = $request->validated();
        $user = User::findOrFail($id);
        $user->update($validatedData);
        if (isset($validatedData['password'])) {
            $user->update([
                'password' => Hash::make($validatedData['password']),
            ]);
        }
        return new JsonResponse(Response::HTTP_OK);
    }

    public function show(User $user): UserResource
    {
        $this->authorize('show', User::class);

        return UserResource::make($user);
    }

    public function destroy(string $id): JsonResponse
    {
        $this->authorize('delete', User::class);

        User::findOrFail($id)->delete();

        return new JsonResponse(Response::HTTP_OK);
    }
}

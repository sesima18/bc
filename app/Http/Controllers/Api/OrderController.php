<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Order\UpdateOrderRequest;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\Product\ProductResource;
use App\Models\Order\Order;
use App\Notifications\Purchase;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    private const PER_PAGE = 1;
    public function index(Request $request): AnonymousResourceCollection
    {
        $this->authorize('index', Order::class);

        $orders = Order::orderBy('id', 'desc');
        if ($request->has('query')) {
            $orders->whereHas('cartItems', function ($query) use ($request) {
                $query->where('name', 'LIKE', '%' . $request->get('query') . '%');
            })->orWhere('name', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('surname', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('email', 'LIKE', '%' . $request->get('query') . '%');
        }

        return OrderResource::collection(
            $orders->paginate(
                $request->get('perPage', self::PER_PAGE)
            )
        );
    }

    public function update(UpdateOrderRequest $request, string $id): JsonResponse
    {
        $this->authorize('update', Order::class);

        $validatedData = $request->validated();
        $order = Order::findOrFail($id);
        $statusUpdated = ($order->status != $validatedData['status']);
        $order->update($validatedData);
        $orderCartItems = $order->cartItems->map(fn($a) => $a->id)->toArray();
        $requestCartItems = array_map(fn($a) => $a['id'], $validatedData['cartItems']);
        $removedCartItems = array_diff($orderCartItems, $requestCartItems);
        $order->cartItems()->whereIn('id', $removedCartItems)->delete();
        if($statusUpdated) {
            $order->notify(new Purchase($order, trans('db.letterOrderStatusSubject'), null, 'emails.status'));
        }
        return new JsonResponse(Response::HTTP_OK);
    }

    public function show(Order $order): OrderResource
    {
        $this->authorize('show', Order::class);

        return OrderResource::make($order);
    }

    public function destroy(Order $order): JsonResponse
    {
        $this->authorize('delete', Order::class);

        $order->cartItems()->delete();
        $order->delete();
        return new JsonResponse(Response::HTTP_OK);
    }
}

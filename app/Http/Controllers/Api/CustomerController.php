<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Customer\UpdateCustomerRequest;
use App\Http\Resources\CustomerResource;
use App\Models\Customer\Customer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Request $request): AnonymousResourceCollection
    {
        $this->authorize('index', Customer::class);

        $customers = Customer::orderBy('id');
        if ($request->has('query')) {
            $customers->where('name', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('surname', 'LIKE', '%' . $request->get('query') . '%')
                ->orWhere('email', 'LIKE', '%' . $request->get('query') . '%');
        }

        return CustomerResource::collection($customers->get());
    }

    public function update(UpdateCustomerRequest $request, string $id): JsonResponse
    {
        $this->authorize('update', Customer::class);

        $validatedData = $request->validated();
        $user = Customer::findOrFail($id);
        $validatedDataDb = $validatedData;
        unset($validatedDataDb['password']);
        $user->update($validatedDataDb);
        if (isset($validatedData['password'])) {
            $user->update([
                'password' => Hash::make($validatedData['password']),
            ]);
        }
        return new JsonResponse(Response::HTTP_OK);
    }

    public function show(Customer $customer): CustomerResource
    {
        $this->authorize('show', Customer::class);

        return CustomerResource::make($customer);
    }

    public function destroy(string $id): JsonResponse
    {
        $this->authorize('delete', Customer::class);

        Customer::findOrFail($id)->delete();

        return new JsonResponse(Response::HTTP_OK);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Helpers\ImageHelper;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product\Product;
use App\Models\Product\ProductCategory;
use App\Models\Product\ProductPhoto;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    private const PER_PAGE = 1;

    public function index(Request $request): AnonymousResourceCollection
    {
        $this->authorize('index', Product::class);

        $products = Product::orderBy('priority');
        if ($request->has('query')) {
            $products->whereHas('descriptions', function ($query) use ($request) {
                $query->where('name', 'LIKE', '%' . $request->get('query') . '%');
            });
        }

        return ProductResource::collection(
            $products->paginate(
                $request->get('perPage', self::PER_PAGE)
            )
        );
    }

    public function store(StoreProductRequest $request): JsonResponse
    {
        $this->authorize('store', Product::class);

        $validatedData = $request->validated();
        $product = Product::create($validatedData);
        $priority = Product::max('priority') + 1;
        $product->update(['priority' => $priority]);
        $this->saveCategories($product, $validatedData);
        $this->saveDescriptions($product, $validatedData);
        $this->addImages($product, $request);
        return new JsonResponse(Response::HTTP_CREATED);
    }

    public function update(UpdateProductRequest $request, string $id): JsonResponse
    {
        $this->authorize('update', Product::class);

        $validatedData = $request->validated();
        $product = Product::findOrFail($id);
        $product->update($validatedData);
        $product->categories()->delete();
        $this->saveCategories($product, $validatedData);
        $this->saveDescriptions($product, $validatedData);
        $product->refresh();
        $this->removeImages($request);
        $this->addImages($product, $request);
        return new JsonResponse(Response::HTTP_OK);
    }

    public function show(Product $product): ProductResource
    {
        $this->authorize('show', Product::class);

        return ProductResource::make($product);
    }

    public function destroy(Product $product): JsonResponse
    {
        $this->authorize('delete', Product::class);
        if($product->images) {
            foreach ($product->images as $img) {
                ImageHelper::removeImage($img->photo, ProductPhoto::IMAGES_PATH);
                $img->delete();
            }
        }
        $product->photos()->delete();
        $product->categories()->delete();
        $product->descriptions()->delete();
        $product->delete();
        return new JsonResponse(Response::HTTP_OK);
    }

    private function saveCategories(Product $product, array $validatedData): void
    {
        if (isset($validatedData['categoriesIds'])) {
            foreach ($validatedData['categoriesIds'] as $categoriesId) {
                $product->categories()->save(ProductCategory::create(['category_id' => $categoriesId]));
            }
        }
    }

    private function saveDescriptions(Product $product, array $validatedData): void
    {
        $descriptions = [];
        if (isset($validatedData['descriptions'])) {
            foreach ($validatedData['descriptions'] as $languageId => $validatedDescription) {
                $productDescription = $product->descriptions()->firstOrCreate(['language_id' => $languageId]);
                $validatedDescription['slug'] = Str::slug($validatedDescription['slug']);
                $productDescription->update($validatedDescription);
                $descriptions[] = $productDescription;
            }
        }
        $product->descriptions()->saveMany($descriptions);
    }

    private function addImages(Product $product, Request $request): void
    {
        if (is_array($request->file('photos')) and !empty($request->file('photos'))) {
            foreach ($request->file('photos') as $photo) {
                $imageName = ImageHelper::uploadImage($product, $photo, ProductPhoto::IMAGES_PATH);
                ProductPhoto::create(['product_id' => $product->id, 'name' => $imageName]);
            }
        }
    }

    private function removeImages(Request $request): void
    {
        if (is_array($request->input('photosRemoved')) and !empty($request->input('photosRemoved'))) {
            foreach ($request->input('photosRemoved') as $photoId) {
                $img = ProductPhoto::find($photoId);
                if ($img) {
                    ImageHelper::removeImage($img->photo, ProductPhoto::IMAGES_PATH);
                    $img->delete();
                }
            }
        }
    }
}

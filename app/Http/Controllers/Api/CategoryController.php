<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Http\Resources\Category\CategoryResource;
use App\Models\Category\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    public function index(): JsonResponse
    {
        $this->authorize('index', Category::class);

        $categories = Category::orderBy('priority')->get();
        return new JsonResponse([
            'categories' => CategoryResource::collection($categories)
        ], Response::HTTP_OK);
    }

    public function store(StoreCategoryRequest $request): JsonResponse
    {
        $this->authorize('store', Category::class);

        $validatedData = $request->validated();
        $category = Category::create($validatedData);
        $priority = Category::where('parent_id', $validatedData['parent_id'])->max('priority') + 1;
        $category->update(['priority' => $priority]);
        $this->saveDescriptions($category, $validatedData);
        return new JsonResponse(Response::HTTP_CREATED);
    }

    public function update(UpdateCategoryRequest $request, string $id): JsonResponse
    {
        $this->authorize('update', Category::class);

        $validatedData = $request->validated();
        $validatedData['parent_id'] = ($validatedData['parent_id'] == 0) ? null : $validatedData['parent_id'];
        $category = Category::findOrFail($id);
        $category->update($validatedData);
        $this->saveDescriptions($category, $validatedData);
        $category->refresh();

        return new JsonResponse(Response::HTTP_OK);
    }

    public function show(Category $category): CategoryResource
    {
        $this->authorize('show', Category::class);

        return CategoryResource::make($category);
    }

    public function destroy(Category $category): JsonResponse
    {
        //todo authorize un deletable categories
        $this->authorize('delete', Category::class);

        //MAX_LEVEL 3
        foreach (Category::where("parent_id", $category->id)->get() as $c) {
            foreach (Category::where("parent_id", $c->id)->get() as $c2) {
                $c2->descriptions()->delete();
                $c2->delete();
            }
            $c->descriptions()->delete();
            $c->delete();
        }

        $category->descriptions()->delete();
        $category->delete();
        return new JsonResponse(Response::HTTP_OK);
    }

    private function saveDescriptions(Category $category, array $validatedData): void
    {
        $descriptions = [];
        if (isset($validatedData['descriptions'])) {
            foreach ($validatedData['descriptions'] as $languageId => $validatedDescription) {
                $categoryDescription = $category->descriptions()->firstOrCreate(['language_id' => $languageId]);
                $validatedDescription['slug'] = Str::slug($validatedDescription['slug']);
                $categoryDescription->update($validatedDescription);
                $descriptions[] = $categoryDescription;
            }
        }
        $category->descriptions()->saveMany($descriptions);
    }

    public function changePriority(Request $request): void
    {
        $priority = 1;
        foreach ($request->get('ids') as $id) {
            if ($id > 0) {
                $item = Category::find($id);
                $item->priority = $priority;
                $item->save();
                $priority++;
            }
        }
    }
}

<?php

namespace App\Services;

use App\Http\Data\DeliveryData;
use App\Http\Data\DeliveryTerminalData;
use App\Models\Delivery\Delivery;
use App\Models\Delivery\DeliveryDescription;
use App\Models\Delivery\DeliveryTerminal;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeliveryService
{
    public function getDeliveryData(Delivery $delivery): DeliveryData
    {
        $deliveryByLocation = $this->getDeliveryByLocation($delivery);
        $termimals = $delivery->terminals->map(function($deliveryTerminal) {
            return (new DeliveryTerminalData())->create($deliveryTerminal);
        })->toArray();
        return (new DeliveryData())->create($delivery, $deliveryByLocation, $termimals);
    }

    public function getDeliveryByLocation(Delivery $Delivery): DeliveryDescription|HasMany
    {
        return $Delivery->descriptions()->where('language_id', session()->get('currentLanguage')->id)->first();
    }

    public function getList(): array
    {
        $list = [];
        foreach (Delivery::all() as $Delivery) {
            $list[] = $this->getDeliveryData($Delivery);
        }
        return $list;
    }

    public function getDeliveryDataById(int $deliveryId): DeliveryData
    {
        $delivery = Delivery::find($deliveryId);
        return $this->getDeliveryData($delivery);
    }

    public function getDeliveryTerminalDataByCode(?string $deliveryTerminalCode = null): ?DeliveryTerminalData
    {
        if(empty($deliveryTerminalCode)) {
            return NULL;
        }
        $deliveryTerminal = DeliveryTerminal::where('code', $deliveryTerminalCode)->first();
        return (new DeliveryTerminalData())->create($deliveryTerminal);
    }
}

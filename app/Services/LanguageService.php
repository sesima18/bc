<?php

namespace App\Services;

use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LanguageService
{
    public function getCurrent(?Request $request = null): Language
    {
        $locale = ($request) ? $request->segment(1) : null;
        $language = ($locale) ? Language::where('name', $locale)->first() : null;
        if(!$language) {
            $language = Language::where('default', 1)->first();
            $locale = $language->name;
        }
        App::setLocale($locale);
        if($request) {
            $request->session()->put('currentLanguage', $language);
        } else {
            session()->put('currentLanguage', $language);
        }
        return $language;
    }
}

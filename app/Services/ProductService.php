<?php

namespace App\Services;

use App\Http\Data\CategoryData;
use App\Http\Data\ProductData;
use App\Models\Category\Category;
use App\Models\Product\Product;
use App\Models\Product\ProductDescription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;

class ProductService
{
    /**
     * @return ProductData[]
     */
    public function getProducts(
        ?CategoryData $currentCategory = null,
        ?string $search = null,
        ?int $limit = null,
        bool $isTop = false,
        bool $isNew = false,
        bool $isBest = false,
        ?int $skipProductId = null
    ) {
        $products = Product::orderBy('priority');
        if ($currentCategory) {
            $products->whereHas('categories', fn($q) => $q->where('category_id', $currentCategory->id));
        }
        if ($search) {
            $products->whereHas('descriptions', fn($q) => $q->where('name', 'like', "%$search%"));
        }
        if ($isTop) {
            $products->where('top', 1);
        }
        if ($isNew) {
            $products->whereDate('created_at', '>', Carbon::today()->subDays(env('PRODUCT_NEW_DAYS')));
        }
        if ($isBest) {
            $products->where('sale', '>', 0);
        }
        if($skipProductId) {
            $products->where('id', '!=', $skipProductId);
        }
        $products->where('hidden', 0);

        $productsList = $products->get();
        if (!$search) {
            $productsList = $productsList->filter(function ($model) {
                return $model->quantityWeb > 0;
            });
        }
        $productsList = $productsList->map(function ($even) {
            return $this->getProductData($even);
        });
        $page = Paginator::resolveCurrentPage() ?: 1;
        if ($limit) {
            $perPage = $limit;
            $productsList = new LengthAwarePaginator(
                $productsList->forPage($page, $perPage),
                $productsList->count(),
                $perPage,
                $page,
                ['path' => Paginator::resolveCurrentPath()]
            );
        }
        if ($search) {
            $productsList->appends(['search' => $search]);
        }

        return $productsList;
    }

    public function getProductDataById(int $productId): ProductData
    {
        $product = Product::find($productId);
        return $this->getProductData($product);
    }

    public function getProductData(Product $product): ProductData
    {
        $productByLocation = $this->getProductByLocation($product);
        $link = $this->getProductLink($productByLocation);
        return (new ProductData())->create($product, $productByLocation, $link);
    }

    public function getProductByLocation(Product $product): ProductDescription|HasMany
    {
        return $product->descriptions()->where('language_id', session()->get('currentLanguage')->id)->first();
    }

    public function getProductLink(ProductDescription $productDescription): string
    {
        $productCategory = $productDescription->product->categories()->first();
        $category = $productCategory->category ?? Category::findOrFail(4);
        $categoryService = new CategoryService();
        $categoryLink = $categoryService->getCategoryLink($category);
        return $categoryLink . '/' . $productDescription->slug;
    }

    public function getCurrent(Request $request): ?ProductData
    {
        $product = null;
        $productData = null;
        $slug = last($request->segments());
        $productDescription = ($slug) ? ProductDescription::where('slug', $slug)
            ->where('language_id', session()->get('currentLanguage')->id)->first() : null;
        if ($productDescription) {
            $product = $productDescription->product;
        }
        if ($product) {
            $productData = $this->getProductData($product);
        }
        return $productData;
    }
}

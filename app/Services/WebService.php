<?php

namespace App\Services;

use App\Helpers\PriceHelper;
use App\Http\Data\CartData;
use App\Http\Data\CategoryData;
use App\Http\Data\CheckoutData;
use App\Http\Data\CustomerData;
use App\Http\Data\OrderCartData;
use App\Http\Data\OrderData;
use App\Http\Data\ProductData;
use App\Http\Data\WebData;
use App\Models\Language;
use App\Models\Product\Product;
use App\ValueObjects\Money;
use Illuminate\Http\Request;
use \Cart;

class WebService
{
    private CategoryData $currentCategory;
    private CategoryService $categoryService;
    private Language $currentLanguage;

    private ProductService $productsService;

    public function __construct(
        CategoryData $currentCategory,
        CategoryService $categoryService,
        Language $currentLanguage
    ) {
        $this->currentCategory = $currentCategory;
        $this->categoryService = $categoryService;
        $this->currentLanguage = $currentLanguage;

        $this->productsService = new ProductService($this->currentLanguage);
    }

    public function getWebData(?array $data = []): array
    {
        $data['menuHeader'] = $this->categoryService->getMenu(null, 'menu_header');
        $data['menuFooter1'] = $this->categoryService->getMenu(null, 'menu_footer_1');
        $data['menuFooter2'] = $this->categoryService->getMenu(null, 'menu_footer_2');

        $data['languages'] = Language::all();
        $data['currentCategory'] = $this->currentCategory;
        $data['currentLocale'] = $this->currentLanguage->name;

        if ($this->currentCategory->id > 1) {
            $data['breadcrumbs'][] = $this->currentCategory->name;
        }
        $data['breadcrumbs'][] = trans('db.pageTitle');
        $data['pageTitle'] = implode(' | ', $data['breadcrumbs']);

        $productsMenu = $this->categoryService->getMenu(4);
        $data['productsMenu'] = $productsMenu;
        $data['searchCategory'] = $this->categoryService->getCategoryDataById(12);

        $data['topCategory'] = $this->categoryService->getCategoryDataById(17);
        $data['newCategory'] = $this->categoryService->getCategoryDataById(16);
        $data['bestCategory'] = $this->categoryService->getCategoryDataById(15);

        $data['cartCategory'] = $this->categoryService->getCategoryDataById(9);
        $data['cartTotal'] = new Money(Cart::total(), '€');
        $data['orderCategory'] = $this->categoryService->getCategoryDataById(10);

        $data['customerCategory'] = $this->categoryService->getCategoryDataById(11);
        $data['currentCustomer'] = \Auth::guard('customer')->user();
        return $data;
    }

    public function getHome(): WebData
    {
        $topProducts = $this->productsService->getProducts(null, null, 3, true);
        $data['topProducts'] = $topProducts;

        $newProducts = $this->productsService->getProducts(null, null, 4, false, true);
        $data['newProducts'] = $newProducts;

        $bestProducts = $this->productsService->getProducts(null, null, 4, false, false, true);
        $data['bestProducts'] = $bestProducts;

        $webData = new WebData();
        $webData->template = 'web/home';
        $webData->data = $this->getWebData($data);
        return $webData;
    }

    public function getIndex(): WebData
    {
        $data = [];
        $webData = new WebData();
        $webData->template = 'web/index';

        $webData->data = $this->getWebData($data);
        return $webData;
    }

    public function getProducts(
        Request $request,
        bool $isTop = false,
        bool $isNew = false,
        bool $isBest = false
    ): WebData {
        $data = [];
        $productCategory = $this->currentCategory;
        if (in_array($this->currentCategory->id, [4, 17, 16, 15])) { //todo
            $productCategory = null;
        }
        $products = $this->productsService->getProducts($productCategory, null, 9, $isTop, $isNew, $isBest);
        $data['products'] = $products;
        $template = 'web/products/index';

        if ($currentProduct = $this->productsService->getCurrent($request)) {
            $data['currentProduct'] = $currentProduct;
            $data['breadcrumbs'][] = $currentProduct->name;
            $productCategory = $this->categoryService->getCategoryDataById($currentProduct->categoryId);
            $data['relatedProducts'] = $this->productsService->getProducts(
                $productCategory,
                null,
                3,
                false,
                false,
                false,
                $data['currentProduct']->id
            );
            $template = 'web/products/view';
        }

        $webData = new WebData();
        $webData->template = $template;
        $webData->data = $this->getWebData($data);
        return $webData;
    }

    public function getSearch(Request $request): WebData
    {
        $products = $this->productsService->getProducts(null, $request->input('search'), 9);
        $data['products'] = $products;
        $data['search'] = $request->input('search');

        $webData = new WebData();
        $webData->template = 'web/products/search';
        $webData->data = $this->getWebData($data);
        return $webData;
    }

    public function getCart(): WebData
    {
        $cartService = new CartService();
        $data['cartItems'] = $cartService->getCartItems();
        $webData = new WebData();
        $webData->template = 'web/checkout/cart';
        $webData->data = $this->getWebData($data);
        return $webData;
    }

    public function getCheckout(Request $request): WebData
    {
        $cartService = new CartService();
        $data['cartItems'] = $cartService->getCartItems();

        $countryService = new CountryService();
        $data['countries'] = $countryService->getList();

        $deliveryService = new DeliveryService();
        $data['deliveries'] = $deliveryService->getList();

        $paymentService = new PaymentService();
        $data['payments'] = $paymentService->getList();

        $checkoutData = new CheckoutData();
        if (\Auth::guard('customer')->check()) {
            $customerData = (new CustomerData())->create(\Auth::guard('customer')->user());
            $checkoutData = (new CheckoutData())->createFromCustomerData($customerData);
        }
        $data['checkoutData'] = $request->session()->get('checkoutData') ?? $checkoutData;

        $data['deliveryId'] = old('deliveryId', $data['checkoutData']->deliveryId ?? null);
        $data['paymentId'] = old('paymentId', $data['checkoutData']->paymentId ?? null);

        $slug = last($request->segments());
        $webData = new WebData();
        $webData->template = 'web/checkout/index';
        if ($slug == 'preview') {
            $webData->template = 'web/checkout/preview';
        }
        if ($slug == 'paid') {
            $webData->template = 'web/checkout/paid';
        }
        $webData->data = $this->getWebData($data);
        return $webData;
    }

    public function getCustomer(Request $request): WebData
    {
        $countryService = new CountryService();
        $data['countries'] = $countryService->getList();

        $webData = new WebData();

        $data['customerData'] = new CustomerData();
        $webData->template = 'web/customer/index';
        $slug = last($request->segments());
        if ($slug == 'forgot-password') { //todo links from customer and checkout to enum
            $webData->template = 'web/customer/forgotPassword';
        }
        if (strpos($request->url(), 'reset-password') !== false) {
            $data['token'] = $slug;
            $webData->template = 'web/customer/resetPassword';
        }

        if (\Auth::guard('customer')->check()) {
            $data['customerData'] = (new CustomerData())->create(\Auth::guard('customer')->user());
            $orders = \Auth::guard('customer')->user()->orders()->orderByDesc('id')->get();
            foreach ($orders as $order) {
                $products = $order->cartItems->map(function($orderCartItem) {
                    $productData = $this->productsService->getProductDataById($orderCartItem->product_id);
                    return (new OrderCartData())->create($orderCartItem, $productData);
                })->toArray();
                $data['orders'][] = (new OrderData)->create($order, $products);
            }
            $webData->template = 'web/customer/profile';
        }

        $webData->data = $this->getWebData($data);
        return $webData;
    }
}

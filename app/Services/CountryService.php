<?php

namespace App\Services;

use App\Http\Data\CountryData;
use App\Models\Country\Country;
use App\Models\Country\CountryDescription;
use Illuminate\Database\Eloquent\Relations\HasMany;

class CountryService
{
    public function getCountryData(Country $country): CountryData
    {
        $countryByLocation = $this->getCountryByLocation($country);
        return (new CountryData())->create($country, $countryByLocation);
    }

    public function getCountryByLocation(Country $country): CountryDescription|HasMany
    {
        return $country->descriptions()->where('language_id', session()->get('currentLanguage')->id)->first();
    }

    public function getList(): array
    {
        $list = [];
        foreach (Country::all() as $country) {
            $list[] = $this->getCountryData($country);
        }
        return $list;
    }

    public function getCountryDataById(int $countryId): CountryData
    {
        $country = Country::find($countryId);
        return $this->getCountryData($country);
    }
}

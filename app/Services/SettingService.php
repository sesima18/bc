<?php

namespace App\Services;

use App\Models\Language;
use App\Models\Setting\Setting;
use Illuminate\Support\Facades\App;

class SettingService
{
    /**
     * Prepare translation.
     *
     * @param string $key
     * @param bool $HTML
     *
     * @return null|string
     * @throws \Exception
     */
    public function getSettingValue($key, $HTML = false)
    {
        if (empty($key)) {
            throw new \Exception('Key is empty.');
        }
        $item = Setting::where('key', $key)->first();
        if ($item) {
            if ($HTML) {
                return $item->translate()->body;
            }
            return strip_tags($item->translate()->body);
        }
        return NULL;
    }

    /**
     * Translation for Laravel Localization module.
     *
     * @param array $group
     * @param string $locale
     *
     * @return array
     */
    public function getGroup($group, $locale)
    {
        $settings = Setting::query()->get()
            ->map(function (Setting $setting) use ($locale, $group) {
                $key = preg_replace("/{$group}\\./", '', $setting->key, 1);
                $keyHTML = $key . '.html';
                $settingByLocation = $this->getSettingByLocation($setting);
                $bodyText = html_entity_decode($settingByLocation->body);
                $body = strip_tags($bodyText);
                $bodyHTML = $bodyText;
                return compact('key', 'body', 'keyHTML', 'bodyHTML');
            });
        $items = $settings->pluck('body', 'key')->toArray();
        $itemsHTML = $settings->pluck('bodyHTML', 'keyHTML')->toArray();
        return array_merge($items, $itemsHTML);
    }

    public function getSettingByLocation(Setting $setting)
    {
        $currentLanguageId = (session()->get('currentLanguage')->id) ?? 1;
        $description = $setting->descriptions()->where('language_id', $currentLanguageId)->firstOrFail();
        return $description;
    }
}

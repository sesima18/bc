<?php

namespace App\Services;

use App\Enums\CategoryTypes;
use App\Http\Data\CategoryData;
use App\Models\Category\Category;
use App\Models\Category\CategoryDescription;
use App\Models\Language;
use App\Models\Product\ProductCategory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class CategoryService
{
    public function getCurrent(Request $request): ?CategoryData
    {
        $category = null;
        $categoryData = null;
        $slug = last($request->segments());
        if (!$slug || Language::where('name', $slug)->first()) {
            $category = Category::find(1);
        }
        $segments = array_reverse($request->segments());
        foreach ($segments as $slug) {
            $categoryDescription = ($slug) ? CategoryDescription::where('slug', $slug)
                ->where('language_id', session()->get('currentLanguage')->id)->first() : null;
            if ($categoryDescription) {
                $category = $categoryDescription->category;
                if ($category->hidden == 1) {
                    continue;
                }
                break;
            }
        }
        if ($category) {
            $categoryData = $this->getCategoryData($category);
        }
        return $categoryData;
    }

    private function getCategoryType(Category $category): ?CategoryTypes
    {
        $categoryType = null;
        if ($category->id == 1) {
            $categoryType = CategoryTypes::HOME;
        }
        if(isset($this->getWebCategories()[$category->id])) {
            $webCategory = $this->getWebCategories()[$category->id];
            $categoryType = CategoryTypes::from($webCategory);
        }
        if (empty($categoryType)) {
            $categoryType = $this->checkCategoryType(
                $category,
                [4], //todo
                CategoryTypes::PRODUCTS
            );
        }
        return $categoryType;
    }

    private function checkCategoryType(
        Category $category,
        array $typeCategories,
        CategoryTypes $categoryType
    ): ?CategoryTypes {
        $isType = count(array_intersect(array_merge([$category->id], $category->ParentsList), $typeCategories)) > 0;
        return $isType ? $categoryType : null;
    }


    /**
     * @return CategoryData[]
     */
    public function getMenu(?int $parentId = null, ?string $menuField = null): array
    {
        $categories = Category::orderBy('priority')->where('hidden', 0);
        if ($parentId > 0) {
            $categories->where('parent_id', $parentId);
        } elseif ($parentId === 0) {
            $categories->whereNull('parent_id');
        }
        if ($menuField) {
            $categories->where($menuField, 1);
        }

        $menu = [];
        foreach ($categories->get() as $category) {
            $menu[] = $this->getCategoryData($category, $this->getMenu($category->id));
        }
        return $menu;
    }

    public function getCategoryDataById(int $categoryId): CategoryData
    {
        $category = Category::find($categoryId);
        return $this->getCategoryData($category);
    }

    public function getCategoryData(Category $category, ?array $submenu = []): CategoryData
    {
        $categoryByLocation = $this->getCategoryByLocation($category);
        $categoryType = $this->getCategoryType($category);
        $link = $this->getCategoryLink($category);
        $isInPath = $this->isCategoryInPath($category);
        return (new CategoryData())->create($category, $categoryByLocation, $categoryType, $link, $isInPath, $submenu);
    }

    public function getCategoryByLocation(Category $category): CategoryDescription|HasMany
    {
        return $category->descriptions()->where('language_id', session()->get('currentLanguage')->id)->first();
    }

    public function getCategoryLink(Category $category): string
    {
        $urls = [];
        if ($category->id > 1) {
            $i = 0;
            while ($category->parent_id > 0 && $i < 5) {
                $categoryByLocation = $this->getCategoryByLocation($category);
                $urls[] = $categoryByLocation->slug;
                $category = $category->parent;
                $i++;
            }
            $categoryByLocation = $this->getCategoryByLocation($category);
            $urls = array_merge($urls, [$categoryByLocation->slug]);
        }
        $urls[] = App::getLocale();
        return '/' . implode("/", array_reverse($urls));
    }

    public function isCategoryInPath(Category $category): bool
    {
        $url = substr($this->getCategoryLink($category), 1);
        $requestEnd = ($category->id == 1) ? '' : '*';
        return (\Request::is($url . $requestEnd));
    }

    public function getWebCategories(): array
    {
        $webCategories = explode(',', env('WEB_CATEGORIES'));
        return array_combine(
            array_map(fn($webCategory) => explode(':', $webCategory)[0], $webCategories),
            array_map(fn($webCategory) => explode(':', $webCategory)[1], $webCategories)
        );
    }
}

<?php

namespace App\Services;

use App\Http\Data\PaymentData;
use App\Models\Payment\Payment;
use App\Models\Payment\PaymentDescription;
use Illuminate\Database\Eloquent\Relations\HasMany;

class PaymentService
{
    public function getPaymentData(Payment $Payment): PaymentData
    {
        $PaymentByLocation = $this->getPaymentByLocation($Payment);
        return (new PaymentData())->create($Payment, $PaymentByLocation);
    }

    public function getPaymentByLocation(Payment $Payment): PaymentDescription|HasMany
    {
        return $Payment->descriptions()->where('language_id', session()->get('currentLanguage')->id)->first();
    }

    public function getList(): array
    {
        $list = [];
        foreach (Payment::all() as $Payment) {
            $list[] = $this->getPaymentData($Payment);
        }
        return $list;
    }

    public function getPaymentDataById(int $paymentId): PaymentData
    {
        $payment = Payment::find($paymentId);
        return $this->getPaymentData($payment);
    }
}

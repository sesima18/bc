<?php

namespace App\Services;

use App\Http\Data\CartData;
use App\Models\Product\Product;
use \Cart;
use Gloudemans\Shoppingcart\CartItem;

class CartService
{
    private ProductService $productsService;

    public function __construct()
    {
        $this->productsService = new ProductService();
    }

    public function getCartItems(): array
    {
        $cartItems = [];
        foreach (Cart::content() as $cartItem) {
            $product = Product::find($cartItem->id);
            $productData = $this->productsService->getProductData($product);
            if($cartItem->qty > $productData->quantity) {
                Cart::update($cartItem->rowId, $productData->quantity);
            }
            $cartItems[] = (new CartData())->create($cartItem, $productData);
        }
        return $cartItems;
    }

    public function addCartItem(int $productId): CartItem
    {
        $product = Product::find($productId);
        $productData = $this->productsService->getProductData($product);
        return Cart::add(
            $productId,
            $productData->name,
            1,
            $productData->priceMoney->price,
            0
        );
    }
}

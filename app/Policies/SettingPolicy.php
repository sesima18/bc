<?php

namespace App\Policies;

use App\Enums\Permissions;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class SettingPolicy
{
    use HandlesAuthorization;

    public function index(): bool
    {
        $user = Auth::user();

        return in_array(
            Permissions::LIST_SETTINGS->value,
            $user->role
                ->permissions
                ->pluck('key')
                ->toArray()
        );
    }

    public function show(): bool
    {
        $user = Auth::user();

        return in_array(
            Permissions::EDIT_SETTING->value,
            $user->role
                ->permissions
                ->pluck('key')
                ->toArray()
        );
    }

    public function store(): bool
    {
        $user = Auth::user();

        return in_array(
            Permissions::CREATE_SETTING->value,
            $user->role
                ->permissions
                ->pluck('key')
                ->toArray()
        );
    }

    public function update(): bool
    {
        $user = Auth::user();

        return in_array(
            Permissions::EDIT_SETTING->value,
            $user->role
                ->permissions
                ->pluck('key')
                ->toArray()
        );
    }

    public function delete(): bool
    {
        $user = Auth::user();

        return in_array(
            Permissions::DELETE_USER->value,
            $user->role
                ->permissions
                ->pluck('key')
                ->toArray()
        );
    }
}

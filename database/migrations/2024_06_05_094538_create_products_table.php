<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->decimal('price', 10)->nullable();
            $table->decimal('sale', 10)->nullable();
            $table->integer('quantity')->default(0);
            $table->boolean('hidden')->default(0);
            $table->integer('priority')->nullable();
            $table->boolean('top')->default(0);

            $table->timestamps();
        });

        Schema::create('products_descriptions', function (Blueprint $table) {
            $table->id();

            $table->foreignId('language_id')->constrained();
            $table->foreignId('product_id')->nullable()->constrained();
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->text('body')->nullable();

            $table->timestamps();
        });

        Schema::create('products_categories', function (Blueprint $table) {
            $table->id();

            $table->foreignId('category_id')->nullable()->constrained();
            $table->foreignId('product_id')->nullable()->constrained();

            $table->timestamps();
        });

        Schema::create('products_photos', function (Blueprint $table) {
            $table->id();

            $table->foreignId('product_id')->nullable()->constrained();
            $table->string('name')->nullable();
            $table->integer('priority')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products_categories');
        Schema::dropIfExists('products_photos');
        Schema::dropIfExists('products_descriptions');
        Schema::dropIfExists('products');
    }
};

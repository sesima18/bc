<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->string('surname')->nullable();
            $table->string('email');
            $table->string('phone')->nullable();
            $table->string('address');
            $table->string('comment')->nullable();
            $table->foreignId('country_id')->constrained();
            $table->foreignId('delivery_id')->constrained();
            $table->foreignId('payment_id')->constrained();
            $table->string('delivery');
            $table->string('payment');
            $table->string('delivery_terminal_code')->nullable();
            $table->string('delivery_terminal_name')->nullable();
            $table->decimal('total', 10)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('track_code')->nullable();
            $table->integer('language_id')->nullable();

            $table->timestamps();
        });

        Schema::create('orders_cart_items', function (Blueprint $table) {
            $table->id();

            $table->foreignId('order_id')->constrained();
            $table->foreignId('product_id')->nullable()->constrained();
            $table->string('name')->nullable();
            $table->integer('quantity')->default(0);
            $table->decimal('price', 10)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
        Schema::dropIfExists('orders_cart_items');
    }
};

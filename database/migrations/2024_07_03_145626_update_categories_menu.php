<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->boolean('menu_header')->default(0);
            $table->boolean('menu_footer_1')->default(0);
            $table->boolean('menu_footer_2')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->removeColumn('menu_header');
            $table->removeColumn('menu_footer_1');
            $table->removeColumn('menu_footer_2');
        });
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->boolean('default')->default(0);
            $table->boolean('hidden')->default(0);
            $table->timestamps();
        });

        Schema::create('countries_descriptions', function (Blueprint $table) {
            $table->id();

            $table->foreignId('language_id')->constrained();
            $table->foreignId('country_id')->nullable()->constrained();
            $table->string('name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('countries');
        Schema::dropIfExists('countries_descriptions');
    }
};

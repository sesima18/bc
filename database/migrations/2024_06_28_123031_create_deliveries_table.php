<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->id();
            $table->string('country_id')->nullable();
            $table->timestamps();
        });

        Schema::create('deliveries_descriptions', function (Blueprint $table) {
            $table->id();

            $table->foreignId('language_id')->constrained();
            $table->foreignId('delivery_id')->nullable()->constrained();
            $table->string('name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('deliveries');
        Schema::dropIfExists('deliveries_descriptions');
    }
};

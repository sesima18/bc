<?php

namespace Database\Seeders;

use App\Enums\Permissions;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RolesPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $role = Role::updateOrCreate(
            [
                'name' => 'Admin',
            ],

        );

        $permissions = [
            Permissions::LIST_USERS,
            Permissions::CREATE_USER,
            Permissions::DELETE_USER,
            Permissions::EDIT_USER,
            Permissions::LIST_ROLES,
            Permissions::CREATE_ROLE,
            Permissions::DELETE_ROLE,
            Permissions::EDIT_ROLE,
        ];
        foreach ($permissions as $permission) {
            Permission::updateOrCreate(
                [
                    'key' => $permission,
                    'role_id' => $role->id,
                ]
            );
        }
    }
}
